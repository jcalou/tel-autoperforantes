<div class=" content row">
  <div class="large-12 columns">
    <h3><?=($editar == "editar")?"Editar art&iacute;culo":"Cargar nuevo art&iacute;culo"?></h3>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <form action="<?=($editar == "editar")?base_url('index.php/articulo/editar/'.$articulo[0]->id):base_url('index.php/articulo/nuevo');?>" method="POST" id="theform">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <div class="large-6 columns">
          <label>C&oacute;digo
            <input type="text" name="codigo" value='<?=($editar == "editar")?$articulo[0]->codigo:""?>' />
          </label>
        </div>
        <div class="large-6 columns">
          <label>Nombre
            <input type="text" name="nombre" value='<?=($editar == "editar")?$articulo[0]->nombre:""?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Tipo
            <select name="tipo" id="tipo">
              <option value="0">Seleccionar tipo de producto</option>
              <option value="Autoperforantes" <?=($editar == "editar" && $articulo[0]->tipo=="Autoperforantes")?"selected":""?>>Autoperforantes</option>
              <option value="Bulones" <?=($editar == "editar"&&$articulo[0]->tipo=="Bulones")?"selected":""?>>Bulones</option>
              <option value="Tirafondos" <?=($editar == "editar"&&$articulo[0]->tipo=="Tirafondos")?"selected":""?>>Tirafondos</option>
              <option value="Ofertas" <?=($editar == "editar"&&$articulo[0]->tipo=="Ofertas")?"selected":""?>>Ofertas</option>
            </select>
          </label>
        </div>
        <div class="large-6 columns">
          <label>Categor&iacute;a
            <select name="categoria" id="categoria">
              <option value="0">Seleccionar categor&iacute;a</option>
              <option value="TEL-HEX" <?=($editar == "editar" && $articulo[0]->categoria=="TEL-HEX")?"selected":""?>>TEL-HEX</option>
              <option value="TEL-DRY" <?=($editar == "editar" && $articulo[0]->categoria=="TEL-DRY")?"selected":""?>>TEL-DRY</option>
              <option value="TEL-ALAS" <?=($editar == "editar" && $articulo[0]->categoria=="TEL-ALAS")?"selected":""?>>TEL-ALAS</option>
              <option value="TEL-PHIL" <?=($editar == "editar" && $articulo[0]->categoria=="TEL-PHIL")?"selected":""?>>TEL-PHIL</option>
              <option value="TEL-FIX" <?=($editar == "editar" && $articulo[0]->categoria=="TEL-FIX")?"selected":""?>>TEL-FIX</option>
              <option value="TEL-PARKER" <?=($editar == "editar" && $articulo[0]->categoria=="TEL-PARKER")?"selected":""?>>TEL-PARKER</option>
              <option value="TEL-WALL" <?=($editar == "editar" && $articulo[0]->categoria=="TEL-WALL")?"selected":""?>>TEL-WALL</option>
              <option value="TARUGOS-TEL" <?=($editar == "editar" && $articulo[0]->categoria=="TARUGOS-TEL")?"selected":""?>>TARUGOS-TEL</option>
              <option value="BULONES" <?=($editar == "editar" && $articulo[0]->categoria=="BULONES")?"selected":""?>>BULONES</option>
            </select>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label>Tipo de Pack
            <select name="tipo_pack" id="tipo_pack">
              <option value="0">Seleccione tipo packaging</option>
              <option value="Caja Grande" <?=($editar == "editar" && $articulo[0]->tipo_pack=="Caja Grande")?"selected":""?>>Caja Grande</option>
              <option value="Estuche" <?=($editar == "editar" && $articulo[0]->tipo_pack=="Estuche")?"selected":""?>>Estuche</option>
              <option value="Mini Estuche" <?=($editar == "editar" && $articulo[0]->tipo_pack=="Mini Estuche")?"selected":""?>>Mini Estuche</option>
              <option value="Ahorro Pack" <?=($editar == "editar" && $articulo[0]->tipo_pack=="Ahorro Pack")?"selected":""?>>Ahorro Pack</option>
            </select>
          </label>
        </div>
        <div class="large-2 columns">
          <label>Cantidad
            <input type="text" name="cantidad" value='<?=($editar == "editar")?$articulo[0]->cantidad:""?>' />
          </label>
        </div>
        <div class="large-2 columns">
          <label>Precio x100
            <input type="text" name="precio_ref_100" value='' />
          </label>
        </div>
        <div class="large-2 columns">
          <label>Precio de referencia
            <input type="text" name="precio_ref" value='<?=($editar == "editar")?$articulo[0]->precio_ref:""?>' />
          </label>
        </div>
        <div class="large-2 columns">
          <label>Stock
            <input type="text" name="stock" value='<?=($editar == "editar")?$articulo[0]->stock:""?>' />
          </label>
        </div>
        <div class="large-2 columns">
          <label>Stock M&iacute;nimo
            <input type="text" name="stock_minimo" value='<?=($editar == "editar")?$articulo[0]->stock_minimo:""?>' />
          </label>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Guardar' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>