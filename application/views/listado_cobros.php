<div class="content row">
    <div class="large-12 columns">
      <h3>Listado de Cobros para la venta numero <?=$id_venta?></h3>
    </div>
</div>

<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Descripcion</th>
          <th>Cobrado</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($cobros);$i++) { ?>
        <tr>
          <td><?=$cobros[$i]->id ?></td>
          <td><?=$cobros[$i]->fecha ?></td>
          <td><?=$cobros[$i]->descripcion ?></td>
          <td>$ <?=$cobros[$i]->cobrado ?></td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>