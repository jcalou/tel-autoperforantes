<div class="content row">
    <div class="large-4 columns">
      <h3>Listado de Cheques</h3>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('cheque/nuevo');?>" class="button postfix">Cargar nuevo Cheque</a>
    </div>
</div>

<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Cliente</th>
          <th>Fecha</th>
          <th>Monto</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($cheques);$i++) { ?>
        <tr>
          <td><?=$cheques[$i]->id ?></td>
          <td><?=$cheques[$i]->nombre_cliente ?></td>
          <td><?=$cheques[$i]->fecha ?></td>
          <td><?=$cheques[$i]->monto ?></td>
          <td>
            <a href="<?=base_url('cheque/editar');?>/<?=$cheques[$i]->id ?>"><span data-tooltip aria-haspopup="true" title="Editar"><i class="fa fa-pencil"></i></span></a>
            <a href="<?=base_url('cheque/eliminar');?>/<?=$cheques[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este cheque?')) { return false; }"><span data-tooltip aria-haspopup="true" title="Eliminar"><i class="fa fa-times"></i></span></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>