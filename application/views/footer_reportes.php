  <footer></footer>
    <script src="<?php echo base_url('assets/js/vendor/jquery-1.10.2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/foundation.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/highcharts.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/exporting.js') ?>"></script>
    <script>
        $(document).foundation();
        $(document).ready(function(){
          $('#graph01').highcharts({
            chart: {
                type: 'column',
                margin: [ 50, 50, 100, 80]
            },
            title: {
                text: 'Total de ventas por mes para los últimos 12 meses'
            },
            xAxis: {
                categories: [
                    '<?=$mes[0]?>',
                    '<?=$mes[1]?>',
                    '<?=$mes[2]?>',
                    '<?=$mes[3]?>',
                    '<?=$mes[4]?>',
                    '<?=$mes[5]?>',
                    '<?=$mes[6]?>',
                    '<?=$mes[7]?>',
                    '<?=$mes[8]?>',
                    '<?=$mes[9]?>',
                    '<?=$mes[10]?>',
                    '<?=$mes[11]?>'
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total de ventas'
                }
            },
            legend: {
                enabled: false
            },
            credits: {
              enabled: false
            },
            tooltip: {
                pointFormat: 'Total: <b>$ {point.y:.1f}</b>',
            },
            series: [{
                name: 'Total',
                data: [
                  <?=$monto[0]; ?>,
                  <?=$monto[1]; ?>,
                  <?=$monto[2]; ?>,
                  <?=$monto[3]; ?>,
                  <?=$monto[4]; ?>,
                  <?=$monto[5]; ?>,
                  <?=$monto[6]; ?>,
                  <?=$monto[7]; ?>,
                  <?=$monto[8]; ?>,
                  <?=$monto[9]; ?>,
                  <?=$monto[10]; ?>,
                  <?=$monto[11]; ?>
                  ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: 4,
                    y: 10,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }
            }]
          });
        });

    </script>
    <script src="<?php echo base_url('assets/js/plugins.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/main.js') ?>"></script>

  </body>
</html>
