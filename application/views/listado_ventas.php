<div class="content row">
    <div class="large-4 columns">
      <h3>Listado de Ventas</h3>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('index.php/venta/nuevo');?>" class="button postfix">Cargar nueva Venta</a>
    </div>
</div>

<form method="POST" action="<?=base_url('index.php/venta/listado'); ?>">
<div class="content row">
  <div class="large-2 columns">
    <label>Desde
      <input type="text" class="span2" name="desde" value="<?=$desde ?>" id="reporte-gastos-dp1">
    </label>
  </div>
  <div class="large-2 columns">
    <label>Hasta
      <input type="text" class="span2" name="hasta" value="<?=$hasta ?>" id="reporte-gastos-dp2">
    </label>
  </div>
  <div class="large-4 columns">
    <label>Cliente
      <?php echo drop_clientes($clientes,$cliente,'','clientes'); ?>
    </label>
  </div>
  <div class="large-2 columns">
    <label>Estado
      <select name="estado" id="estado">
        <option value="todas" <?=($estado=='todas')?'selected':''?>>Todas</option>
        <option value="pagadas" <?=($estado=='pagadas')?'selected':''?>>Pagadas</option>
        <option value="pendientes" <?=($estado=='pendientes')?'selected':''?>>Pendientes</option>
      </select>
    </label>
  </div>
  <div class="large-2 columns">
    <input type="hidden" name="post" value="1" />
    <input type="submit" value='Buscar' class="button success" />
  </div>
</div>
</form>

<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>Nro.</th>
          <th>Cliente</th>
          <th>Fecha</th>
          <th>Total Factura</th>
          <th>Cobrado</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($ventas);$i++) { 
        $show = true;
        if(cobrado($ventas[$i]->id)=="$ "){
          $style = 'style="background-color:#f0d4d4"';
          switch ($estado) {
            case "pagadas":
              $show = false;
              break;
            case "pendientes":
              $show = true;
              break;
          }
        }else{
          $style = '';
          switch ($estado) {
            case "pagadas":
              $show = true;
              break;
            case "pendientes":
              $show = false;
              break;
          }
        }

        if ($show){

        ?>
        <tr <?=$style?>>
          <td><?=$ventas[$i]->id ?></td>
          <td><?=$ventas[$i]->nombre_cliente ?></td>
          <td><?=$ventas[$i]->fecha ?></td>
          <td>$ <?=$ventas[$i]->total ?></td>
          <?php if(cobrado($ventas[$i]->id)!="$ "){?>
            <td><?=cobrado($ventas[$i]->id) ?> <a href="<?=base_url('index.php/venta/detallecobro');?>/<?=$ventas[$i]->id ?>" style="font-size: 9px;">detalle</a></td>
          <?php }else{?>
            <td>-</td>
          <?php }?>
          <td>
            <a href="<?=base_url('index.php/venta/ver');?>/<?=$ventas[$i]->id ?>"><span data-tooltip aria-haspopup="true" title="Ver detalle"><i class="fa fa-eye"></i></span></a>
            <a href="<?=base_url('index.php/venta/cobro');?>/<?=$ventas[$i]->id ?>"><span data-tooltip aria-haspopup="true" title="Cargar un pago"><i class="fa fa-money"></i></span></a>
            <a href="<?=base_url('index.php/venta/imprimir');?>/<?=$ventas[$i]->id ?>" target="_blank"><span data-tooltip aria-haspopup="true" title="Imprimir"><i class="fa fa-print"></i></span></a>
            <a href="<?=base_url('index.php/venta/eliminar');?>/<?=$ventas[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta venta? Si elige que si se actualizara el stock con los productos de la venta.')) { return false; }"><span data-tooltip aria-haspopup="true" title="Cancelar factura"><i class="fa fa-times"></i></span></a>
          </td>
        </tr>
      <?php } }; ?>
      </tbody>
    </table>
  </div>
</div>
<style type="text/css">
table thead tr th, table thead tr td, table tbody tr th, table tbody tr td a i.fa, table tbody tr td {
  font-size: 12px;
}
</style>
<div class="content row">
    <div class="large-4 columns">
      <a href="<?=base_url('index.php/venta/listado_imprimir/'.$desde.'/'.$hasta.'/'.$cliente.'/'.$estado.'');?>" class="button postfix" target="_blank">Imprimir Listado</a>
    </div>
</div>
