<div class=" content row">
  <div class="large-12 columns">
    <h3><?=($editar == "editar")?"Editar Venta":"Cargar Nueva Venta"?></h3>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <div class="row">
      <div class="large-6 columns">
        <label>Cliente
          <?php
          if ($editar == "editar"){$cid=$venta[0]->id_cliente;}else{$cid=0;}
          echo drop_clientes($clientes,$cid,"","cliente"); ?>
        </label>
      </div>
      <div class="large-1 columns">
        <label>Auto.
          <input type="text" value="<?=($editar == "editar")?$venta[0]->d1:""?>" name="d1" id="d1" disabled />
        </label>
      </div>
      <div class="large-1 columns">
        <label>Bulon.
          <input type="text" value="<?=($editar == "editar")?$venta[0]->d2:""?>" name="d2" id="d2" disabled />
        </label>
      </div>
      <div class="large-1 columns">
        <label>Tiraf.
          <input type="text" value="<?=($editar == "editar")?$venta[0]->d3:""?>" name="d3" id="d3" disabled />
        </label>
      </div>
      <div class="large-1 columns">
        <label>Oferta
          <input type="text" value="<?=($editar == "editar")?$venta[0]->d4:""?>" name="d4" id="d4" disabled />
        </label>
      </div>
      <div class="large-2 columns">
        <label>Fecha
          <input id="dp3" type="text" value="<?=($editar == "editar")?$venta[0]->fecha:date('Y-m-d')?>" name="fecha" />
        </label>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="large-1 columns">
        <label>Cantidad
          <input type="text" id="cantidad" name="cantidad" value="1" />
        </label>
      </div>
      <div class="large-3 columns">
        <label>Producto
          <?php echo drop_articulos($articulos,0,"","articulo"); ?>
        </label>
        <input type="text" id="id_hidden" style="display:none;opacity:0;" />
      </div>
      <div class="large-2 columns">
        <label>Precio Referencia
          <input type="text" id="preciounitario" />
        </label>
      </div>
      <div class="large-1 columns">
        <label>Coef.
          <input type="text" id="coeficiente" disabled />
        </label>
      </div>
      <div class="large-1 columns">
        <label>Desc.
          <input type="text" id="coeficientecliente" disabled />
        </label>
      </div>
      <div class="large-2 columns">
        <label>Total
          <input type="text" id="preciototal" disabled />
        </label>
      </div>
      <div class="large-1 columns">
        <label>&nbsp;
          <a href="#" class="button postfix" id="agregaritem">Agregar</a>
        </label>
      </div>
      <div class="large-1 columns">
        <label>&nbsp;
          <a href="" class="button postfix" id="limpiaritem">Limpiar</a>
        </label>
      </div>
    </div>
    <hr>
    <div class="content row">
      <div class="large-8 columns">
        <table width="100%" id="items">
          <thead>
            <tr>
              <th>Producto</th>
              <th width="55">$ x U</th>
              <th width="55">Cant.</th>
              <th width="30">Total</th>
              <th width="30">Eliminar</th>
              <th class="item_id"></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <label>Observaciones / Comentarios
          <textarea name="observaciones" id="observaciones"></textarea>
        </label>
      </div>
      <div class="large-4 columns">
        <div class="large-12 columns">
          <label>Subtotal
            <h3 id="subtotal">$ 0,00</h3>
          </label>
          <label>IVA (21%)
            <h3 id="iva">$ 0,00</h3>
          </label>
          <fieldset class="total">
            <legend>TOTAL</legend>
            <h1 id="total">$ 0,00</h1>
          </fieldset>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="large-4 columns">
        <input type="submit" id="aceptarventa" value='Guardar' class="button success" />
        <a href="javascript:history.back();" id="cancelarventa" class="button secondary">Cancelar</a>
      </div>
    </div>

  </div>
</div>
