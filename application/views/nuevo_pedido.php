<div class=" content row">
  <div class="large-8 columns">
    <h3>Nuevo Pedido</h3>
  </div>
  <div class="large-4 columns">
    <label>Fecha
      <input id="pedido_dp" type="text" value="<?=date('Y-m-d')?>" name="pedido_dp" />
    </label>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <div class="row">
      <div class="large-1 columns">
        <label>Cantidad
          <input type="text" id="pedido_cantidad" name="pedido_cantidad" value="1"/>
        </label>
      </div>
      <div class="large-3 columns">
        <label>Producto
          <?php echo drop_articulos($articulos,0,"","pedido_articulo"); ?>
          <input type="text" id="id_hidden" style="display:none;opacity:0;" />
        </label>
      </div>
      <div class="large-2 columns">
        <label>Precio Referencia
          <input type="text" id="pedido_preciounitario" />
        </label>
      </div>
      <div class="large-1 columns">
        <label>Coef.
          <input type="text" id="pedido_coeficiente" disabled />
        </label>
      </div>
      <div class="large-1 columns">
        <label>Desc.
          <input type="text" id="pedido_coeficientecliente" disabled />
        </label>
      </div>
      <div class="large-2 columns">
        <label>Total
          <input type="text" id="pedido_preciototal" disabled />
        </label>
      </div>
      <div class="large-1 columns">
        <label>&nbsp;
          <a href="#" class="button postfix" id="pedido_agregaritem">Agregar</a>
        </label>
      </div>
      <div class="large-1 columns">
        <label>&nbsp;
          <a href="" class="button postfix" id="pedido_limpiaritem">Limpiar</a>
        </label>
      </div>
    </div>
    <hr>
    <div class="content row">
      <div class="large-8 columns">
        <table width="100%" id="items">
          <thead>
            <tr>
              <th>Producto</th>
              <th width="55">$ x U</th>
              <th width="55">Cant.</th>
              <th width="30">Total</th>
              <th width="30">Eliminar</th>
              <th class="item_id"></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <label>Observaciones / Comentarios
          <textarea name="observaciones" id="pedido_observaciones"></textarea>
        </label>
      </div>
      <div class="large-4 columns">
        <div class="large-12 columns">
          <label>Subtotal
            <h3 id="pedido_subtotal">$ 0,00</h3>
          </label>
          <label>IVA (21%)
            <h3 id="pedido_iva">$ 0,00</h3>
          </label>
          <fieldset class="total">
            <legend>TOTAL</legend>
            <h1 id="pedido_total">$ 0,00</h1>
          </fieldset>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="large-4 columns">
        <input type="submit" id="pedido_aceptarventa" value='Guardar' class="button success" />
        <a href="javascript:history.back();" id="pedido_cancelarventa" class="button secondary">Cancelar</a>
      </div>
    </div>

  </div>
</div>
