<form action="self" method="POST">
<div class=" content row">
  <div class="large-8 columns">
    <h3>Nuevo Cobro para la Venta id:<?=$venta?></h3>
  </div>
</div>
<div class="row">
  <div class="large-4 columns">
    <label>Fecha
      <input id="cobro_dp" type="text" value="<?=date('Y-m-d')?>" name="cobro_dp" />
    </label>
  </div>
  <div class="large-6 columns">
    <label>Descripci&oacute;n
      <input type="text" id="pago_descripcion" name="pago_descripcion" />
    </label>
  </div>
  <div class="large-2 columns">
    <label>Total Cobrado
      <input type="text" id="pago_preciototal" name="pago_preciototal" />
    </label>
  </div>
</div>
<div class="row">
  <div class="large-4 columns">
    <input type="submit" value='Guardar' class="button success" />
    <a href="javascript:history.back();" id="ppago_cancelarventa" class="button secondary">Cancelar</a>
  </div>
<input type="hidden" name="post" value="1" />
<input type="hidden" name="id_venta" value="<?=$venta?>" />
</div>
</form>