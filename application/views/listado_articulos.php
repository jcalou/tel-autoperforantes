<div class="content row">
  <form action="<?=base_url('articulo/listado');?>" method="GET">
    <div class="large-4 columns">
      <h3>Listado de Art&iacute;culos</h3>
    </div>
    <div class="large-4 columns">
      <div class="row collapse">
        <div class="large-10 columns">
          <input type="text" placeholder="Nombre o codigo del articulo" name="q" id="q" required />
        </div>
        <div class="large-2 columns">
          <input type="submit" value="Buscar" class="button postfix" />
        </div>
      </div>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('index.php/articulo/nuevo');?>" class="button postfix">Cargar nuevo Art&iacute;culo</a>
    </div>
  </form>
</div>

<div class="content row">
  <?php
    if(isset($q)){
      ?>
  <div class="large-12 columns">
    <h4>Filtrando por "<?=$q;?>"</h4>
  </div>
      <?php
    } ?>
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>Codigo</th>
          <th>Nombre</th>
          <td>Tipo</td>
          <th>Cantidad</th>
          <th>Precio Ref.</th>
          <th>Stock</th>
          <th>St. min</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($articulos);$i++) { ?>
        <tr class="<?=($articulos[$i]->stock < 1)?"sinstock":""?> <?=($articulos[$i]->stock-$articulos[$i]->stock_minimo < 1)?"stockmin":""?> ">
          <td><?=$articulos[$i]->codigo ?></td>
          <td><?=$articulos[$i]->nombre ?></td>
          <td><?=$articulos[$i]->tipo ?></td>
          <td><?=$articulos[$i]->cantidad ?></td>
          <td><?=$articulos[$i]->precio_ref ?></td>
          <td><?=$articulos[$i]->stock ?></td>
          <td><?=$articulos[$i]->stock_minimo ?></td>
          <td style="width:75px;">
            <a href="<?=base_url('index.php/articulo/editar');?>/<?=$articulos[$i]->id ?>"><span data-tooltip aria-haspopup="true" title="Editar"><i class="fa fa-pencil"></i></span></a>
            <a href="<?=base_url('index.php/articulo/eliminar');?>/<?=$articulos[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este art&iacute;culo?')) { return false; }"><span data-tooltip aria-haspopup="true" title="Eliminar"><i class="fa fa-times"></i></span></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>