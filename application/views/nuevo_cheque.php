<div class=" content row">
  <div class="large-12 columns">
    <h3><?=($editar == "editar")?"Editar Cheque":"Cargar nuevo Cheque"?></h3>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <form action="<?=($editar == "editar")?base_url('cheque/editar/'.$cheque[0]->id):base_url('cheque/nuevo');?>" method="POST" id="theform">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <div class="large-4 columns">
          <label>Cliente
            <?php
            if ($editar == "editar"){$cid=$cheque[0]->id_cliente;}else{$cid=0;}
            echo drop_clientes($clientes,$cid,"","cheques_cliente"); ?>
          </label>
        </div>
        <div class="large-4 columns">
          <label>Fecha
            <input id="dp3" type="text" value="<?=($editar == "editar")?$cheque[0]->fecha:""?>" name="fecha" />
          </label>
        </div>
        <div class="large-4 columns">
          <label>Monto
            <input type="text" name="monto" placeholder='$$$' value="<?=($editar == "editar")?$cheque[0]->monto:""?>"/>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Numero
            <input type="text" name="numero" value="<?=($editar == "editar")?$cheque[0]->numero:""?>" />
          </label>
        </div>
        <div class="large-6 columns">
          <label>Banco
            <input type="text" name="banco" value="<?=($editar == "editar")?$cheque[0]->banco:""?>" />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <label>Observaciones
            <textarea name="observaciones" id="observaciones" cols="30" rows="10"><?=($editar == "editar")?$cheque[0]->observaciones:""?></textarea>
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Guardar' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>