<div class=" content row">
  <div class="large-12 columns">
    <h3 style="text-align:center;">Modificar coeficientes actuales</h3>
  </div>
</div>
<form action="<?=base_url('configuracion/modificar_coeficientes');?>" method="POST">
<?php for($i=0;$i<count($coeficientes);$i++) { ?>
<div class="content row">
  <div class="large-4 columns">
    &nbsp;
  </div>
  <div class="large-2 columns" style="text-align: right">
    <p style="font-size: 20px;"><?=$coeficientes[$i]->nombre ?></p>
  </div>
  <div class="large-2 columns">
    <input type="text" name="<?=$coeficientes[$i]->nombre ?>" value="<?=$coeficientes[$i]->coeficiente ?>" />
  </div>
  <div class="large-4 columns">
    &nbsp;
  </div>
</div>
<?php }; ?>
<div class="content row">
  <div class="large-4 columns centered">
    &nbsp;
  </div>
  <div class="large-4 columns centered">
    <input type="hidden" name="post" value="1" />
    <input type="submit"class="button postfix" value="Guardar cambios"/>
  </div>
  <div class="large-4 columns centered">
    &nbsp;
  </div>
</div>
</form>