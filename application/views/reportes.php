<div <div class="row content">
  <dl class="tabs" data-tab>
    <dd class="active"><a href="#panel1">Ventas</a></dd>
    <dd><a href="#panel2">Stock</a></dd>
    <dd><a href="#panel3">IVA del mes</a></dd>
  </dl>
  <div class="tabs-content">
    <div class="content active" id="panel1">
      <div class="large-12 columns">
        <h2>Ventas</h2>
      </div>
      <div class="large-12 columns">
        <div id="graph01"></div>
      </div>
    </div>
    <div class="content" id="panel2">
      <div class="large-12 columns">
        <h2>Stock</h2>
        <p>Cantidad diferente de articulos en stock: <b><?php echo $productos_en_stock[0]->dif;?></b><br>
        Cantidad total de articulos en stock: <b><?php echo $productos_en_stock[0]->total;?></b><br>
        <?php
        $monto = 0;
        for($i=0;$i<count($monto_productos_en_stock);$i++) {
          $monto = $monto + ($monto_productos_en_stock[$i]->stock*$monto_productos_en_stock[$i]->precio_ref);
        }
        ?>
        Monto: <b>$ <?php echo number_format($monto,2,",",".");?></b> &ndash; IVA: <b>$ <?php echo number_format($monto*0.21,2,",",".");?></b></p>
      </div>
    </div>
    <div class="content" id="panel3">
      <h2>IVA del mes actual</h2>
      <p>IVA Compras: <b><?php echo $iva_compra;?></b><br>
      IVA Ventas: <b><?php echo $iva_venta;?></b><br>
      Diferencia: <b><?php echo ($iva_compra-$iva_venta);?></b></p>
    </div>
  </div>
</div>
