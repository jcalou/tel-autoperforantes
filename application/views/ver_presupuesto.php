<div class=" content row">
  <div class="large-12 columns">
    <h3>Presupuesto Nro. <?=$presupuesto[0]->id?></h3>
  </div>
</div>
<div class="content row">
  <div class="large-7 columns">
    <h4>Cliente: <?=nombre_cliente($presupuesto[0]->id_cliente);?></h4>
  </div>
  <div class="large-5 columns">
    <h4>Fecha: <?=$presupuesto[0]->fecha?></h4>
  </div>
</div>
<div class="content row">
  <div class="large-3 columns">
    <p>Descuento Aut.: <?=$presupuesto[0]->des_auto;?></p>
  </div>
  <div class="large-3 columns">
    <p>Descuento Bul.: <?=$presupuesto[0]->des_bul;?></p>
  </div>
  <div class="large-3 columns">
    <p>Descuento Tir..: <?=$presupuesto[0]->des_tir;?></p>
  </div>
  <div class="large-3 columns">
    <p>Descuento Ofe.: <?=$presupuesto[0]->des_ofe;?></p>
  </div>
</div>
<div class="content row">
  <div class="large-8 columns">
    <table width="100%" id="items">
      <thead>
        <tr>
          <th>Producto</th>
          <th width="55">$ x U</th>
          <th width="55">Cant.</th>
          <th width="30">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $sub = 0;
        for($i=0;$i<count($presupuesto_detalle);$i++) { ?>
        <tr>
          <td><?=$presupuesto_detalle[$i]->nombre_articulo ?></td>
          <td><?=($presupuesto_detalle[$i]->total / $presupuesto_detalle[$i]->cantidad) ?></td>
          <td><?=$presupuesto_detalle[$i]->cantidad ?></td>
          <td><?=$presupuesto_detalle[$i]->total ?></td>
        </tr>
        <?php
        $sub = $sub + $presupuesto_detalle[$i]->total;
        } ?>
      </tbody>
    </table>
    <p>Observaciones: <?=$presupuesto[0]->observaciones?></p>
  </div>
  <div class="large-4 columns">
    <div class="large-12 columns">
      <label>Subtotal
        <h3 id="subtotal">$ <?=$sub?></h3>
      </label>
      <label>IVA (21%)
        <h3 id="iva">$ <?=$presupuesto[0]->iva?></h3>
      </label>
      <fieldset class="total">
        <legend>TOTAL</legend>
        <h1 id="total">$ <?=$presupuesto[0]->total?></h1>
      </fieldset>
    </div>
  </div>
</div>

<div class="content row">
  <div class="large-4 columns">
    <a href="<?=base_url('presupuesto/generarfactura');?>/<?=$presupuesto[0]->id?>" id="generar" class="button">Generar Factura</a>
    <a href="javascript:history.back();" id="volver" class="button secondary">Volver</a>
  </div>
</div>
