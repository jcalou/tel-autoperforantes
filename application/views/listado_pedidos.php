<div class="content row">
    <div class="large-4 columns">
      <h3>Listado de Compras</h3>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('pedido/nuevo');?>" class="button postfix">Cargar nuevo Pedido</a>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('pedido/nuevo_2');?>" class="button postfix">Cargar nueva Compra</a>
    </div>
</div>

<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Tipo</th>
          <th>Fecha</th>
          <th>IVA</th>
          <th>Total</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($pedidos);$i++) { ?>
        <tr>
          <td><?=$pedidos[$i]->id ?></td>
          <td><?=$pedidos[$i]->tipo ?></td>
          <td><?=$pedidos[$i]->fecha ?></td>
          <td>$ <?=$pedidos[$i]->iva ?></td>
          <td>$ <?=$pedidos[$i]->total ?></td>
          <td>
            <a href="<?=base_url('pedido/ver');?>/<?=$pedidos[$i]->id ?>"><span data-tooltip aria-haspopup="true" title="Ver detalle"><i class="fa fa-eye"></i></span></a>
            <a href="<?=base_url('pedido/imprimir');?>/<?=$pedidos[$i]->id ?>" target="_blank"><span data-tooltip aria-haspopup="true" title="Imprimir"><i class="fa fa-print"></i></span></a>
            <a href="<?=base_url('pedido/eliminar');?>/<?=$pedidos[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar esta compra?')) { return false; }"><span data-tooltip aria-haspopup="true" title="Eliminar"><i class="fa fa-times"></i></span></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>