<div class="content row">
    <div class="large-4 columns">
      <h3>Listado de Presupuestos</h3>
    </div>
    <div class="large-4 columns">
      <a href="<?=base_url('presupuesto/nuevo');?>" class="button postfix">Cargar nuevo Presupuesto</a>
    </div>
</div>

<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Cliente</th>
          <th>Fecha</th>
          <th>IVA</th>
          <th>Total</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($presupuestos);$i++) { ?>
        <tr>
          <td><?=$presupuestos[$i]->id ?></td>
          <td><?=$presupuestos[$i]->nombre_cliente ?></td>
          <td><?=$presupuestos[$i]->fecha ?></td>
          <td>$ <?=$presupuestos[$i]->iva ?></td>
          <td>$ <?=$presupuestos[$i]->total ?></td>
          <td>
            <a href="<?=base_url('index.php/presupuesto/ver');?>/<?=$presupuestos[$i]->id ?>"><span data-tooltip aria-haspopup="true" title="Ver detalle"><i class="fa fa-eye"></i></span></a>
            <a href="<?=base_url('index.php/presupuesto/imprimir');?>/<?=$presupuestos[$i]->id ?>" target="_blank"><span data-tooltip aria-haspopup="true" title="Imprimir"><i class="fa fa-print"></i></span></a>
            <a href="<?=base_url('index.php/presupuesto/eliminar');?>/<?=$presupuestos[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este presupuesto?')) { return false; }"><span data-tooltip aria-haspopup="true" title="Eliminar"><i class="fa fa-times"></i></span></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>