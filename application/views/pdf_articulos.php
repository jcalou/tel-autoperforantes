<?php
  tcpdf();
  $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $obj_pdf->SetCreator(PDF_CREATOR);
  $title = "Listado de Articulos";
  $obj_pdf->SetTitle($title);
  $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
  $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  $obj_pdf->SetDefaultMonospacedFont('helvetica');
  $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  $obj_pdf->SetFont('helvetica', '', 8);
  $obj_pdf->setFontSubsetting(false);
  $obj_pdf->AddPage();
  ob_start();
?>
<h1>Listado de Articulos</h1>
<table width="100%">
  <thead>
    <tr>
      <th>Codigo</th>
      <th>Nombre</th>
      <th>Tipo</th>
      <th>Categoria</th>
      <th>Precio Ref.</th>
      <th>Stock</th>
    </tr>
  </thead>
  <tbody>
  <?php for($i=0;$i<count($articulos);$i++) { ?>
    <tr>
      <td><?=$articulos[$i]->codigo ?></td>
      <td><?=$articulos[$i]->nombre ?></td>
      <td><?=$articulos[$i]->tipo ?></td>
      <td><?=$articulos[$i]->categoria ?></td>
      <td><?=$articulos[$i]->precio_ref ?></td>
      <td><?=$articulos[$i]->stock ?></td>
    </tr>
  <?php }; ?>
  </tbody>
</table>
<?php
  $content = ob_get_contents();
  ob_end_clean();
  $obj_pdf->writeHTML($content, true, false, true, false, '');
  $obj_pdf->Output('articulos_stock.pdf', 'I');
?>
