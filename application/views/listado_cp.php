<div class="content row">
  <div class="large-8 columns">
    <h3>Listado de c&oacute;digos postales</h3>
  </div>
  <div class="large-4 columns">
    <a href="<?=base_url('configuracion/nuevocp');?>" class="button postfix">Cargar nuevo CP</a>
  </div>
</div>
<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>CP</th>
          <th>Localidad</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($cps);$i++) { ?>
        <tr>
          <td><?=$cps[$i]->cp ?></td>
          <td><?=$cps[$i]->localidad ?></td>
          <td>
            <a href="<?=base_url('configuracion/editarcp');?>/<?=$cps[$i]->id ?>"><span data-tooltip aria-haspopup="true" title="Editar"><i class="fa fa-pencil"></i></span></a>
            <a href="<?=base_url('configuracion/eliminarcp');?>/<?=$cps[$i]->id ?>" onclick="if (! confirm('¿Est&aacute; seguro que desea eliminar este elemento?')) { return false; }"><span data-tooltip aria-haspopup="true" title="Eliminar"><i class="fa fa-times"></i></span></a>
          </td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>
