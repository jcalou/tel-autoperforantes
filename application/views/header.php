<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title><?php echo $title; ?></title>
  <meta name="description" content="<?php echo $description; ?>">

  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/favicon.png') ?>" />
  <link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.min.css') ?>" />
  <link rel='stylesheet' href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" />
  <link rel='stylesheet' href="<?php echo base_url('assets/css/style.css') ?>" />

  <script src="<?php echo base_url('assets/js/vendor/modernizr-2.6.2.min.js') ?>"></script>

</head>
  <body>
    <nav class="top-bar" data-topbar>
      <ul class="title-area">
        <li class="name">
          <h1><a href="<?=base_url();?>">TLC</a></h1>
        </li>
      </ul>

      <section class="top-bar-section">
        <ul class="left">
          <li class="has-dropdown <?php echo ($section=='articulos')?'active':''; ?>">
            <a href="<?=base_url('index.php/articulo/listado');?>"><i class="fa fa-wrench"></i>Art&iacute;culos</a>
            <ul class="dropdown">
              <li><a href="<?=base_url('index.php/articulo/listado');?>">Listar</a></li>
              <li><a href="<?=base_url('index.php/articulo/listado?q=_stock');?>">Art&iacute;culos en stock</a></li>
              <li><a href="<?=base_url('index.php/articulo/nuevo');?>">Agregar</a></li>
              <li><a href="<?=base_url('index.php/articulo/imprimir');?>">Imprimir Listados</a></li>
            </ul>
          </li>
          <li class="has-dropdown <?php echo ($section=='clientes')?'active':''; ?>">
            <a href="<?=base_url('index.php/cliente/listado');?>"><i class="fa fa-users"></i>Clientes</a>
            <ul class="dropdown">
              <li><a href="<?=base_url('index.php/cliente/listado');?>">Listar</a></li>
              <li><a href="<?=base_url('index.php/cliente/nuevo');?>">Agregar</a></li>
            </ul>
          </li>
          <li class="has-dropdown <?php echo ($section=='presupuestos')?'active':''; ?>">
            <a href="<?=base_url('index.php/presupuesto/listado');?>"><i class="fa fa-file-powerpoint-o"></i>Presupuestos</a>
            <ul class="dropdown">
              <li><a href="<?=base_url('index.php/presupuesto/listado');?>">Listar</a></li>
              <li><a href="<?=base_url('index.php/presupuesto/nuevo');?>">Agregar</a></li>
            </ul>
          </li>
          <li class="has-dropdown <?php echo ($section=='ventas')?'active':''; ?>">
            <a href="<?=base_url('index.php/venta/listado');?>"><i class="fa fa-money"></i>Ventas</a>
            <ul class="dropdown">
              <li><a href="<?=base_url('index.php/venta/listado');?>">Listar</a></li>
              <li><a href="<?=base_url('index.php/venta/nuevo');?>">Agregar</a></li>
            </ul>
          </li>
          <li class="has-dropdown <?php echo ($section=='pedidos')?'active':''; ?>">
            <a href="<?=base_url('index.php/pedido/listado');?>"><i class="fa fa-shopping-cart"></i>Compras</a>
            <ul class="dropdown">
              <li><a href="<?=base_url('index.php/pedido/listado');?>">Listar Compras</a></li>
              <li><a href="<?=base_url('index.php/pedido/nuevo');?>">Agregar Pedido</a></li>
              <li><a href="<?=base_url('index.php/pedido/nuevo_2');?>">Agregar Otra Compra</a></li>
            </ul>
          </li>
          <li class="has-dropdown <?php echo ($section=='cheques')?'active':''; ?>">
            <a href="<?=base_url('index.php/cheque/listado');?>"><i class="fa fa-keyboard-o"></i>Cheques</a>
            <ul class="dropdown">
              <li><a href="<?=base_url('index.php/cheque/listado');?>">Listar</a></li>
              <li><a href="<?=base_url('index.php/cheque/nuevo');?>">Agregar</a></li>
            </ul>
          </li>
          <li class="<?php echo ($section=='reportes')?'active':''; ?>">
            <a href="<?=base_url('index.php/reporte/main');?>"><i class="fa fa-signal"></i>Reportes</a>
          </li>
          <li class="has-dropdown <?php echo ($section=='configuracion')?'active':''; ?>">
            <a href="<?=base_url('index.php/configuracion/coeficientes');?>"><i class="fa fa-cogs"></i>Configuraci&oacute;n</a>
            <ul class="dropdown">
              <li><a href="<?=base_url('index.php/configuracion/coeficientes');?>">Coeficientes</a></li>
              <li><a href="<?=base_url('index.php/configuracion/cps');?>">C&oacute;digos postales</a></li>
            </ul>
          </li>
        </ul>
        <ul class="right">
          <li><p>Fecha: <?=date("d-m-Y");?></p></li>
        </ul>
      </section>
    </nav>
