<form action="<?=base_url('pedido/nuevo_2');?>" method="POST">
<div class=" content row">
  <div class="large-8 columns">
    <h3>Nueva Compra</h3>
  </div>
  <div class="large-4 columns">
    <label>Fecha
      <input id="pedido_dp" type="text" value="<?=date('Y-m-d')?>" name="pedido_dp" />
    </label>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <div class="row">
      <div class="large-8 columns">
        <label>Descripci&oacute;n
          <input type="text" id="compra_descripcion" name="compra_descripcion" />
        </label>
      </div>
      <div class="large-2 columns">
        <label>IVA
          <input type="text" id="compra_iva" name="compra_iva" />
        </label>
      </div>
      <div class="large-2 columns">
        <label>Total
          <input type="text" id="compra_preciototal" name="compra_preciototal" />
        </label>
      </div>
    </div>

    <div class="row">
      <div class="large-4 columns">
        <input type="submit" value='Guardar' class="button success" />
        <a href="javascript:history.back();" id="pedido_cancelarventa" class="button secondary">Cancelar</a>
      </div>
    </div>
    <input type="hidden" name="post" id="" value="1" />

  </div>
</div>
</form>