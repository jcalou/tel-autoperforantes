<div class=" content row">
  <div class="large-12 columns">
    <h3>Compra Nro. <?=$pedido[0]->id?></h3>
  </div>
</div>
<div class="content row">
  <div class="large-7 columns">
    <h4>Tipo: <?=$pedido[0]->tipo;?></h4>
  </div>
  <div class="large-5 columns">
    <h4>Fecha: <?=$pedido[0]->fecha?></h4>
  </div>
</div>
<div class="content row">
  <div class="large-8 columns">
    <?php if($pedido[0]->tipo=="pedido"){?>
    <table width="100%" id="items">
      <thead>
        <tr>
          <th>Producto</th>
          <th width="55">$ x U</th>
          <th width="55">Cant.</th>
          <th width="30">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $sub = 0;
        for($i=0;$i<count($pedido_detalle);$i++) { ?>
        <tr>
          <td><?=$pedido_detalle[$i]->nombre_articulo ?></td>
          <td><?=($pedido_detalle[$i]->precio / $pedido_detalle[$i]->cantidad) ?></td>
          <td><?=$pedido_detalle[$i]->cantidad ?></td>
          <td><?=$pedido_detalle[$i]->precio ?></td>
        </tr>
        <?php
        $sub = $sub + $pedido_detalle[$i]->precio;
        } ?>
      </tbody>
    </table>
    <?php }?>
    <p>Observaciones: <?=$pedido[0]->observaciones?></p>
  </div>
  <div class="large-4 columns">
    <div class="large-12 columns">
      <?php if($pedido[0]->tipo=="pedido"){ ?>
      <label>Subtotal
        <h3 id="subtotal">$ <?=$sub?></h3>
      </label>
      <?php }?>
      <label>IVA (21%)
        <h3 id="iva">$ <?=$pedido[0]->iva?></h3>
      </label>
      <fieldset class="total">
        <legend>TOTAL</legend>
        <h1 id="total">$ <?=$pedido[0]->total?></h1>
      </fieldset>
    </div>
  </div>
</div>

<div class="content row">
  <div class="large-4 columns">
    <a href="javascript:history.back();" id="volver" class="button secondary">Volver</a>
  </div>
</div>
