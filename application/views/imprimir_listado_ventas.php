<?php
  tcpdf();
  $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $obj_pdf->SetCreator(PDF_CREATOR);
  $title = "Listado de ventas " . $desde . ' - ' . $hasta;
  $obj_pdf->SetTitle($title);
  $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
  $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  $obj_pdf->SetDefaultMonospacedFont('helvetica');
  $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  $obj_pdf->SetFont('helvetica', '', 8);
  $obj_pdf->setFontSubsetting(false);
  $obj_pdf->AddPage();
  ob_start();
?>
<p>Cliente: <?=$cliente?></p>
<p>Estado: <?=$estado ?></p>
<table width="100%">
  <thead>
    <tr>
      <th width="10%">Nro.</th>
      <th width="50%">Cliente</th>
      <th width="15%">Fecha</th>
      <th width="15%">Total Factura</th>
      <th width="10%">Cobrado</th>
    </tr>
  </thead>
  <tbody>
  <?php for($i=0;$i<count($ventas);$i++) { 
    $show = true;
    if(cobrado($ventas[$i]->id)=="$ "){
      $style = 'style="background-color:#f0d4d4"';
      switch ($estado) {
        case "pagadas":
          $show = false;
          break;
        case "pendientes":
          $show = true;
          break;
      }
    }else{
      $style = '';
      switch ($estado) {
        case "pagadas":
          $show = true;
          break;
        case "pendientes":
          $show = false;
          break;
      }
    }

    if ($show){

    ?>
    <tr <?=$style?>>
      <td width="10%"><?=$ventas[$i]->id ?></td>
      <td width="50%"><?=$ventas[$i]->nombre_cliente ?></td>
      <td width="15%"><?=$ventas[$i]->fecha ?></td>
      <td width="15%">$ <?=$ventas[$i]->total ?></td>
      <?php if(cobrado($ventas[$i]->id)!="$ "){?>
        <td width="10%"><?=cobrado($ventas[$i]->id) ?> </td>
      <?php }else{?>
        <td width="10%">-</td>
      <?php }?>
    </tr>
  <?php } }; ?>
  </tbody>
</table>
<?php
  $content = ob_get_contents();
  ob_end_clean();
  $obj_pdf->writeHTML($content, true, false, true, false, '');
  $obj_pdf->Output('articulos_stock.pdf', 'I');
?>
