<div class="row">
  <div class="large-12 columns">
    <h1>Coeficientes actuales <span class="edit"><a href="<?=base_url('index.php/configuracion/coeficientes');?>">modificar</a></span></h1>
  </div>
  <div class="large-4 columns">
    <div class="coeficiente c1">
      <div class="icon"><i class="fa fa-cog"></i></div>
      <h2><?=$coeficientes[0]->nombre ?></h2>
      <p><?=$coeficientes[0]->coeficiente ?>%</p>
    </div>
  </div>
  <div class="large-4 columns">
    <div class="coeficiente c2">
      <div class="icon"><i class="fa fa-cog"></i></div>
      <h2><?=$coeficientes[1]->nombre ?></h2>
      <p><?=$coeficientes[1]->coeficiente ?>%</p>
    </div>
  </div>
  <div class="large-4 columns">
    <div class="coeficiente c3">
      <div class="icon"><i class="fa fa-cog"></i></div>
      <h2><?=$coeficientes[2]->nombre ?></h2>
      <p><?=$coeficientes[2]->coeficiente ?>%</p>
    </div>
  </div>
</div>
