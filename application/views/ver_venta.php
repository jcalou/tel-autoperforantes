<div class=" content row">
  <div class="large-12 columns">
    <h3>Venta Nro. <?=$venta[0]->id?></h3>
  </div>
</div>
<div class="content row">
  <div class="large-7 columns">
    <h4>Cliente: <?=nombre_cliente($venta[0]->id_cliente);?></h4>
  </div>
  <div class="large-5 columns">
    <h4>Fecha: <?=$venta[0]->fecha?></h4>
  </div>
</div>
<div class="content row">
  <div class="large-3 columns">
    <p>Descuento Aut.: <?=$venta[0]->des_auto;?></p>
  </div>
  <div class="large-3 columns">
    <p>Descuento Bul.: <?=$venta[0]->des_bul;?></p>
  </div>
  <div class="large-3 columns">
    <p>Descuento Tir..: <?=$venta[0]->des_tir;?></p>
  </div>
  <div class="large-3 columns">
    <p>Descuento Ofe.: <?=$venta[0]->des_ofe;?></p>
  </div>
</div>
<div class="content row">
  <div class="large-8 columns">
    <table width="100%" id="items">
      <thead>
        <tr>
          <th>Producto</th>
          <th width="55">$ x U</th>
          <th width="55">Cant.</th>
          <th width="30">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $sub = 0;
        for($i=0;$i<count($venta_detalle);$i++) { ?>
        <tr>
          <td><?=$venta_detalle[$i]->nombre_articulo ?></td>
          <td><?=($venta_detalle[$i]->total / $venta_detalle[$i]->cantidad) ?></td>
          <td><?=$venta_detalle[$i]->cantidad ?></td>
          <td><?=$venta_detalle[$i]->total ?></td>
        </tr>
        <?php
        $sub = $sub + $venta_detalle[$i]->total;
        } ?>
      </tbody>
    </table>
    <p>Observaciones: <?=$venta[0]->observaciones?></p>
  </div>
  <div class="large-4 columns">
    <div class="large-12 columns">
      <label>Subtotal
        <h3 id="subtotal">$ <?=$sub?></h3>
      </label>
      <label>IVA (21%)
        <h3 id="iva">$ <?=$venta[0]->iva?></h3>
      </label>
      <fieldset class="total">
        <legend>TOTAL</legend>
        <h1 id="total">$ <?=$venta[0]->total?></h1>
      </fieldset>
    </div>
  </div>
</div>
<hr>
<div class=" content row">
  <div class="large-12 columns">
    <h3>Cobros realizados para esta factura</h3>
  </div>
</div>
<div class="content row">
  <div class="large-12 columns">
    <table width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Fecha</th>
          <th>Descripcion</th>
          <th>Cobrado</th>
        </tr>
      </thead>
      <tbody>
      <?php for($i=0;$i<count($cobros);$i++) { ?>
        <tr>
          <td><?=$cobros[$i]->id ?></td>
          <td><?=$cobros[$i]->fecha ?></td>
          <td><?=$cobros[$i]->descripcion ?></td>
          <td>$ <?=$cobros[$i]->cobrado ?></td>
        </tr>
      <?php }; ?>
      </tbody>
    </table>
  </div>
</div>
<div class="content row">
  <div class="large-4 columns">
    <a href="javascript:history.back();" id="volver" class="button secondary">Volver</a>
  </div>
</div>
