<div class=" content row">
  <div class="large-12 columns">
    <h3><?=($editar == "editar")?"Editar cliente":"Cargar nuevo cliente"?></h3>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <form action="<?=($editar == "editar")?base_url('cliente/editar/'.$cliente[0]->id):base_url('cliente/nuevo');?>" method="POST" id="theform">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <div class="large-6 columns">
          <label>Nombre
            <input type="text" name="nombre" value='<?=($editar == "editar")?$cliente[0]->nombre:""?>' />
          </label>
        </div>
        <div class="large-6 columns">
          <label>Direccion
            <input type="text" name="direccion" value='<?=($editar == "editar")?$cliente[0]->direccion:""?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <label>Localidad
            <?php
            $look = '';
            if ($editar == "editar") {
              $look = $cliente[0]->localidad;
            } 
            echo drop_localidades($localidades,0,'') 
            ?>
          </label>
        </div>
        <div class="large-6 columns">
          <label>Tel&eacute;fono
            <input type="text" name="telefono" value='<?=($editar == "editar")?$cliente[0]->telefono:""?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-3 columns">
          <label>D Autoperforantes
            <input type="text" name="d1" value='<?=($editar == "editar")?$cliente[0]->d1:""?>' />
          </label>
        </div>
        <div class="large-3 columns">
          <label>D Bulones
            <input type="text" name="d2" value='<?=($editar == "editar")?$cliente[0]->d2:""?>' />
          </label>
        </div>
        <div class="large-3 columns">
          <label>D Tirafondos
            <input type="text" name="d3" value='<?=($editar == "editar")?$cliente[0]->d3:""?>' />
          </label>
        </div>
        <div class="large-3 columns">
          <label>D Ofertas
            <input type="text" name="d4" value='<?=($editar == "editar")?$cliente[0]->d4:""?>' />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Guardar' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>