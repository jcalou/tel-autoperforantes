<?php
  tcpdf();
  $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $obj_pdf->SetCreator(PDF_CREATOR);
  $title = "Presupuesto Nro. ".$presupuesto[0]->id;
  $obj_pdf->SetTitle($title);
  $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
  $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  $obj_pdf->SetDefaultMonospacedFont('helvetica');
  $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  $obj_pdf->SetFont('helvetica', '', 8);
  $obj_pdf->setFontSubsetting(false);
  $obj_pdf->AddPage();
  ob_start();
?>
<h4>Fecha: <?=$presupuesto[0]->fecha?></h4>
<h4>Cliente: <?=nombre_cliente($presupuesto[0]->id_cliente);?></h4>
<p>Descuento Aut.: <?=$presupuesto[0]->des_auto;?> - Descuento Bul.: <?=$presupuesto[0]->des_bul;?> - Descuento Tir..: <?=$presupuesto[0]->des_tir;?> - Descuento Ofe.: <?=$presupuesto[0]->des_ofe;?></p>
<table width="100%" id="items">
    <tr>
      <th>Producto</th>
      <th width="55">$ x U</th>
      <th width="55">Cant.</th>
      <th width="30">Total</th>
    </tr>
    <?php
    $sub = 0;
    for($i=0;$i<count($presupuesto_detalle);$i++) { ?>
    <tr>
      <td><?=$presupuesto_detalle[$i]->nombre_articulo ?></td>
      <td><?=($presupuesto_detalle[$i]->total / $presupuesto_detalle[$i]->cantidad) ?></td>
      <td><?=$presupuesto_detalle[$i]->cantidad ?></td>
      <td><?=$presupuesto_detalle[$i]->total ?></td>
    </tr>
    <?php
    $sub = $sub + $presupuesto_detalle[$i]->total;
    } ?>
</table>
<p>Observaciones: <?=$presupuesto[0]->observaciones?></p>
<h3>Subtotal: $ <?=$sub?></h3>
<h3>IVA (21%): $ <?=$presupuesto[0]->iva?></h3>
<h2>TOTAL: $ <?=$presupuesto[0]->total?></h2>
<?php
  $content = ob_get_contents();
  ob_end_clean();
  $obj_pdf->writeHTML($content, true, false, true, false, '');
  $obj_pdf->Output('articulos_stock.pdf', 'I');
?>
