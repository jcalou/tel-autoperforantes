<div class=" content row">
  <div class="large-12 columns">
    <h3><?=($editar == "editar")?"Editar C&oacute;digo Postal":"Cargar nuevo C&oacute;digo Postal"?></h3>
  </div>
</div>
<div class=" content row">
  <div class="large-12 columns">
    <form action="<?=($editar == "editar")?base_url('configuracion/editarcp/'.$cp[0]->id):base_url('configuracion/nuevocp');?>" method="POST" id="theform">
      <input type="hidden" name="id" value="0">
      <div class="row">
        <div class="large-6 columns">
          <label>Localidad
            <input type="text" name="localidad" value="<?=($editar == "editar")?$cp[0]->localidad:""?>" />
          </label>
        </div>
        <div class="large-6 columns">
          <label>CP
            <input type="text" name="cp" value="<?=($editar == "editar")?$cp[0]->cp:""?>" />
          </label>
        </div>
      </div>
      <div class="row">
        <div class="large-4 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" value='Guardar' class="button success" />
          <a href="javascript:history.back();" class="button secondary">Cancelar</a>
        </div>
      </div>

    </form>
  </div>
</div>