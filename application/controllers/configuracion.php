<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracion extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        $this->load->model('config_model');
    }

    public function coeficientes()
    {
        $data['title'] = 'Modificar Coeficientes - TEL - Autoperforantes';
        $data['description'] = 'Modificar Coeficientes - TEL - Autoperforantes';
        $data['section'] = 'configuracion';

        $data['coeficientes'] = $this->config_model->get_coeficientes();

        $this->load->view('header', $data);
        $this->load->view('modificar_coeficientes', $data);
        $this->load->view('footer');
    }

    public function modificar_coeficientes()
    {
        if($this->input->post('post') && $this->input->post('post')==1)
        {
                $coeficientes = array();
                $coeficientes[0] = $this->input->post('Autoperforantes');
                $coeficientes[1] = $this->input->post('Bulones');
                $coeficientes[2] = $this->input->post('Tirafondos');
                $coeficientes[3] = $this->input->post('Ofertas');

                $result = $this->config_model->modificar_coeficientes($coeficientes);
                redirect('');
                return TRUE;
        }
        redirect('configuracion/coeficientes');
        return TRUE;
    }

    public function cps()
    {
        $data['title'] = 'Listado de c&oacute;digos postales - TEL - Autoperforantes';
        $data['description'] = 'Listado de c&oacute;digos postales - TEL - Autoperforantes';
        $data['section'] = 'configuracion';

        $data['cps'] = $this->config_model->get_cps();

        $this->load->view('header', $data);
        $this->load->view('listado_cp', $data);
        $this->load->view('footer');
    }

    public function nuevocp()
    {
        $data['title'] = 'Cargar nuevo C&oacute;digo Postal - TEL - Autoperforantes';
        $data['description'] = 'Cargar nuevo C&oacute;digo Postal - TEL - Autoperforantes';
        $data['section'] = 'configuracion';
        $data['editar'] = 'nuevo';

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $loc = $this->input->post('localidad');
            $cp = $this->input->post('cp');

            $result = $this->config_model->agregar_cp($loc,$cp);
            redirect('configuracion/cps');
            exit;
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_cp');
            $this->load->view('footer');
        }
    }

    public function editarcp($id)
    {
        $data['title'] = 'Editar C&oacute;digo Postal - TEL - Autoperforantes';
        $data['description'] = 'Editar C&oacute;digo Postal - TEL - Autoperforantes';
        $data['section'] = 'configuracion';
        $data['editar'] = 'editar';
        $data['cp'] = $this->config_model->get_cp($id);

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //editar
            $loc = $this->input->post('localidad');
            $cp = $this->input->post('cp');

            $result = $this->config_model->editar_cp($id,$loc,$cp);
            redirect('configuracion/cps');
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_cp');
            $this->load->view('footer');
        }
    }

    public function eliminarcp($id)
    {
        $data['cp'] = $this->config_model->eliminar_cp($id);
        redirect('configuracion/cps');
    }

}
