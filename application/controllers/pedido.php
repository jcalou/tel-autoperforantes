<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedido extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pedido_model');
        $this->load->model('articulo_model');
        }

    public function listado()
    {
        $data['title'] = 'Listado de pedidos - TEL - Autoperforantes';
        $data['description'] = 'Listado de pedidos - TEL - Autoperforantes';
        $data['section'] = 'pedidos';

        $data['pedidos'] = $this->pedido_model->get_pedidos();

        $this->load->view('header', $data);
        $this->load->view('listado_pedidos', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $data['title'] = 'Cargar nuevo pedidos - TEL - Autoperforantes';
        $data['description'] = 'Cargar nuevo pedidos - TEL - Autoperforantes';
        $data['section'] = 'pedidos';
        $data['editar'] = 'nuevo';
        $data['articulos'] = $this->articulo_model->get_articulos_drop();

        $this->load->view('header', $data);
        $this->load->view('nuevo_pedido');
        $this->load->view('footer');
    }

    public function nuevo_2()
    {
        $data['title'] = 'Cargar nueva compra - TEL - Autoperforantes';
        $data['description'] = 'Cargar nueva compra - TEL - Autoperforantes';
        $data['section'] = 'pedidos';
        $data['editar'] = 'nuevo';
        $data['articulos'] = $this->articulo_model->get_articulos_drop();

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $this->pedido_model->tipo = "compra";
            $this->pedido_model->fecha = $this->input->post('pedido_dp');
            $this->pedido_model->iva = $this->input->post('compra_iva');
            $this->pedido_model->observaciones = $this->input->post('compra_descripcion');
            $this->pedido_model->total = $this->input->post('compra_preciototal');

            $agregar = $this->pedido_model->agregar_pedido();

            redirect('pedido/listado');
            exit;
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_pedido_2');
            $this->load->view('footer');
        }
    }

    public function ver($id)
    {
        $data['title'] = 'Ver Compra - TEL - Autoperforantes';
        $data['description'] = 'Ver Compra - TEL - Autoperforantes';
        $data['section'] = 'pedidos';
        $data['pedido'] = $this->pedido_model->get_pedido($id);
        $data['pedido_detalle'] = $this->pedido_model->get_pedido_detalle($id);

        $this->load->view('header', $data);
        $this->load->view('ver_compra', $data);
        $this->load->view('footer');
    }

    public function imprimir($id)
    {
        $data['pedido'] = $this->pedido_model->get_pedido($id);
        $data['pedido_detalle'] = $this->pedido_model->get_pedido_detalle($id);

        $this->load->helper('pdf_helper');
        $this->load->view('imprimir_compra', $data);
    }

    public function eliminar($id)
    {
        $data['pedidos'] = $this->pedido_model->eliminar_pedido($id);
        redirect('pedido/listado');
    }

    public function ajax_agregar()
    {
        $this->pedido_model->tipo = $this->input->post('tipo');
        $this->pedido_model->fecha = $this->input->post('fecha');
        $this->pedido_model->iva = $this->input->post('iva');
        $this->pedido_model->observaciones = $this->input->post('observaciones');
        $this->pedido_model->total = $this->input->post('total');

        $agregar = $this->pedido_model->agregar_pedido();

        echo $agregar;
    }

    public function ajax_agregar_detalle()
    {
        $this->pedido_model->id_pedido = $this->input->post('id_pedido');
        $this->pedido_model->id_articulo = $this->input->post('id_articulo');
        $this->pedido_model->nombre_articulo = $this->input->post('nombre_articulo');
        $this->pedido_model->cantidad = $this->input->post('cantidad');
        $this->pedido_model->precio = $this->input->post('precio');

        $agregar = $this->pedido_model->agregar_pedido_detalle();

        echo $agregar;
    }

}
