<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venta extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('venta_model');
        $this->load->model('cobro_model');
        $this->load->model('cliente_model');
        $this->load->model('articulo_model');
        }

    public function listado($desde="-",$hasta="-",$cliente="0",$estado='-')
    {
        $data['title'] = 'Listado de ventas - TEL - Autoperforantes';
        $data['description'] = 'Listado de ventas - TEL - Autoperforantes';
        $data['section'] = 'ventas';

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            
            $desde = $this->input->post('desde');
            if ($desde=="" || $desde=='-'){$desde=date('Y-m-01');}
            $hasta = $this->input->post('hasta');
            if ($hasta==""){$hasta="-";}
            $cliente = $this->input->post('cliente');
            if ($cliente==""){$cliente="0";}
            $estado = $this->input->post('estado');
            if ($estado==""){$estado="-";}

            redirect('index.php/venta/listado/'.$desde.'/'.$hasta.'/'.$cliente.'/'.$estado.'');
            exit;
        }else{

            if ($desde=="" || $desde=='-'){$desde=date('Y-m-01');}

            $data['ventas'] = $this->venta_model->get_ventas(rawurldecode($desde),rawurldecode($hasta),rawurldecode($cliente));
            $data['clientes'] = $this->cliente_model->get_clientes_drop();

            $data['desde'] = rawurldecode($desde);
            $data['hasta'] = rawurldecode($hasta);
            $data['cliente'] = rawurldecode($cliente);
            $data['estado'] = rawurldecode($estado);

            $this->load->view('header', $data);
            $this->load->view('listado_ventas', $data);
            $this->load->view('footer');
        }
    }

    public function listado_imprimir($desde="-",$hasta="-",$cliente="0",$estado='-')
    {
        $data['title'] = 'Listado de ventas - TEL - Autoperforantes';
        $data['description'] = 'Listado de ventas - TEL - Autoperforantes';
        $data['section'] = 'ventas';

        if ($desde=="" || $desde=='-'){$desde=date('Y-m-01');}

        $data['ventas'] = $this->venta_model->get_ventas(rawurldecode($desde),rawurldecode($hasta),rawurldecode($cliente));
        $data['clientes'] = $this->cliente_model->get_clientes_drop();

        $data['desde'] = rawurldecode($desde);
        $data['hasta'] = rawurldecode($hasta);
        $data['cliente'] = rawurldecode($cliente);
        $data['estado'] = rawurldecode($estado);

        $this->load->helper('pdf_helper');
        $this->load->view('imprimir_listado_ventas', $data);
        
    }

    public function nuevo()
    {
        $data['title'] = 'Cargar nueva Venta - TEL - Autoperforantes';
        $data['description'] = 'Cargar nueva Venta - TEL - Autoperforantes';
        $data['section'] = 'ventas';
        $data['editar'] = 'nuevo';
        $data['clientes'] = $this->cliente_model->get_clientes_drop();
        $data['articulos'] = $this->articulo_model->get_articulos_drop();

        $this->load->view('header', $data);
        $this->load->view('nueva_venta');
        $this->load->view('footer');
    }

    public function ver($id)
    {
        $data['title'] = 'Ver Venta - TEL - Autoperforantes';
        $data['description'] = 'Ver Venta - TEL - Autoperforantes';
        $data['section'] = 'ventas';
        $data['venta'] = $this->venta_model->get_venta($id);
        $data['venta_detalle'] = $this->venta_model->get_venta_detalle($id);
        $data['cobros'] =  $cobro = $this->cobro_model->get_cobro_lista($id);

        $this->load->view('header', $data);
        $this->load->view('ver_venta', $data);
        $this->load->view('footer');
    }

    public function cobro($id)
    {
        $data['title'] = 'Cargar nuevo Cobro - TEL - Autoperforantes';
        $data['description'] = 'Cargar nuevo Cobro - TEL - Autoperforantes';
        $data['section'] = 'ventas';
        $data['editar'] = 'nuevo';
        $data['venta'] = $id;

         if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $this->cobro_model->id_venta = $this->input->post('id_venta');
            $this->cobro_model->fecha = $this->input->post('cobro_dp');
            $this->cobro_model->descripcion = $this->input->post('pago_descripcion');
            $this->cobro_model->cobrado = $this->input->post('pago_preciototal');

            $cobro = $this->cobro_model->agregar_cobro();

            redirect('venta/listado');
            exit;
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_cobro');
            $this->load->view('footer');
        }
    }

    public function detallecobro($id)
    {
        $data['title'] = 'Detalle de cobros para una venta - TEL - Autoperforantes';
        $data['description'] = 'Detalle de cobros para una venta - TEL - Autoperforantes';
        $data['section'] = 'ventas';

        $data['id_venta'] = $id;
        $data['cobros'] =  $cobro = $this->cobro_model->get_cobro_lista($id);

        $this->load->view('header', $data);
        $this->load->view('listado_cobros', $data);
        $this->load->view('footer');
    }

    public function imprimir($id)
    {
        $data['venta'] = $this->venta_model->get_venta($id);
        $data['venta_detalle'] = $this->venta_model->get_venta_detalle($id);

        $this->load->helper('pdf_helper');
        $this->load->view('imprimir_venta', $data);
    }

    public function eliminar($id)
    {
        // restar stock
        $data['venta'] = $this->venta_model->eliminar_venta($id);
        redirect('venta/listado');
    }

    public function ajax_agregar()
    {
        $this->venta_model->id_cliente = $this->input->post('id_cliente');
        $this->venta_model->des_auto = $this->input->post('des_auto');
        $this->venta_model->des_bul = $this->input->post('des_bul');
        $this->venta_model->des_tir = $this->input->post('des_tir');
        $this->venta_model->des_ofe = $this->input->post('des_ofe');
        $this->venta_model->fecha = $this->input->post('fecha');
        $this->venta_model->iva = $this->input->post('iva');
        $this->venta_model->observaciones = $this->input->post('observaciones');
        $this->venta_model->total = $this->input->post('total');

        $agregar = $this->venta_model->agregar_venta();

        echo $agregar;
    }

    public function ajax_agregar_detalle()
    {
        $this->venta_model->id_venta = $this->input->post('id_venta');
        $this->venta_model->id_articulo = $this->input->post('id_articulo');
        $this->venta_model->nombre_articulo = $this->input->post('nombre_articulo');
        $this->venta_model->cantidad = $this->input->post('cantidad');
        $this->venta_model->total = $this->input->post('total');

        $agregar = $this->venta_model->agregar_venta_detalle();

        echo $agregar;
    }

}
