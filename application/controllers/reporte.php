<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporte extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('articulo_model');
        $this->load->model('venta_model');
    }

    public function main()
    {
        $data['title'] = 'Reportes - TEL - Autoperforantes';
        $data['description'] = 'Reportes - TEL - Autoperforantes';
        $data['section'] = 'reportes';
        $data['productos_en_stock'] = $this->articulo_model->productos_en_stock();
        $data['monto_productos_en_stock'] = $this->articulo_model->buscar_articulos_stock();

        $mes = array();
        $m = strftime('%m');
        $y = strftime('%Y');
        $mes[0] = "$y-$m";
        $monto_temp = $this->venta_model->total_mensual($mes[0]);
        $monto[0] = $monto_temp[0]->total;

        for($i=1; $i<12; $i++){
            $m--;
            if($m <= 0){
                $y--;
                $m = 12;
            }
            $mes[$i] ="$y-$m";
            $monto_temp = $this->venta_model->total_mensual($mes[$i]);
            $monto[$i] = $monto_temp[0]->total;
        }
        $data['mes'] = $mes;
        $data['monto'] = $monto;
        $data['iva_compra'] = "200";
        $data['iva_venta'] = "300";

        $this->load->view('header', $data);
        $this->load->view('reportes', $data);
        $this->load->view('footer_reportes', $data);
    }

}
