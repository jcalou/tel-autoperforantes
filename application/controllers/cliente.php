<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cliente extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        $this->load->model('cliente_model');
        }

    public function listado()
    {
        $data['title'] = 'Listado de Clientes - TEL - Autoperforantes';
        $data['description'] = 'Listado de Clientes - TEL - Autoperforantes';
        $data['section'] = 'clientes';

        if($this->input->get('q')) {
            $data['clientes'] = $this->cliente_model->buscar_clientes($this->input->get('q'));
            $data['q'] = $this->input->get('q');

            $this->load->view('header', $data);
            $this->load->view('listado_clientes', $data);
            $this->load->view('footer');
        }else{
            $data['clientes'] = $this->cliente_model->get_clientes();
            $this->load->view('header', $data);
            $this->load->view('listado_clientes', $data);
            $this->load->view('footer');
        }

    }

    public function nuevo()
    {
        $data['title'] = 'Cargar nuevo Cliente - TEL - Autoperforantes';
        $data['description'] = 'Cargar nuevo Cliente - TEL - Autoperforantes';
        $data['section'] = 'clientes';
        $data['editar'] = 'nuevo';

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $this->cliente_model->nombre = $this->input->post('nombre');
            $this->cliente_model->direccion = $this->input->post('direccion');
            $this->cliente_model->localidad = $this->input->post('localidad');
            $this->cliente_model->telefono = $this->input->post('telefono');
            $this->cliente_model->d1 = $this->input->post('d1');
            $this->cliente_model->d2 = $this->input->post('d2');
            $this->cliente_model->d3 = $this->input->post('d3');
            $this->cliente_model->d4 = $this->input->post('d4');

            $result = $this->cliente_model->agregar_cliente();
            redirect('cliente/listado');
            exit;
        }else{
            $this->load->model('config_model');
            $data['localidades'] = $this->config_model->get_cps();

            $this->load->view('header', $data);
            $this->load->view('nuevo_cliente');
            $this->load->view('footer');
        }
    }

    public function editar($id)
    {
        $data['title'] = 'Editar Cliente - TEL - Autoperforantes';
        $data['description'] = 'Editar Cliente - TEL - Autoperforantes';
        $data['section'] = 'clientes';
        $data['editar'] = 'editar';
        $data['cliente'] = $this->cliente_model->get_cliente($id);

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //editar
            $this->cliente_model->nombre = $this->input->post('nombre');
            $this->cliente_model->direccion = $this->input->post('direccion');
            $this->cliente_model->localidad = $this->input->post('localidad');
            $this->cliente_model->telefono = $this->input->post('telefono');
            $this->cliente_model->d1 = $this->input->post('d1');
            $this->cliente_model->d2 = $this->input->post('d2');
            $this->cliente_model->d3 = $this->input->post('d3');
            $this->cliente_model->d4 = $this->input->post('d4');

            $result = $this->cliente_model->editar_cliente($id);
            redirect('cliente/listado');
        }else{
            $this->load->model('config_model');
            $data['localidades'] = $this->config_model->get_cps();

            $this->load->view('header', $data);
            $this->load->view('nuevo_cliente');
            $this->load->view('footer');
        }
    }

    public function eliminar($id)
    {
        $data['cliente'] = $this->cliente_model->eliminar_cliente($id);
        redirect('cliente/listado');
    }

    public function ajax_descuento($id)
    {
        if ($id ==0){
            echo "0";
        }else{
            $cliente = $this->cliente_model->get_cliente($id);
            echo $cliente[0]->d1 . ',' . $cliente[0]->d2 . ',' . $cliente[0]->d3 . ',' . $cliente[0]->d4;
        }
    }

}
