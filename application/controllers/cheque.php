<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cheque extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        $this->load->model('cheque_model');
        $this->load->model('cliente_model');
        }

    public function listado()
    {
        $data['title'] = 'Listado de Cheques - TEL - Autoperforantes';
        $data['description'] = 'Listado de Cheques - TEL - Autoperforantes';
        $data['section'] = 'cheques';

        $data['cheques'] = $this->cheque_model->get_cheques();

        $this->load->view('header', $data);
        $this->load->view('listado_cheques', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $data['title'] = 'Cargar nuevo Cheque - TEL - Autoperforantes';
        $data['description'] = 'Cargar nuevo Cheque - TEL - Autoperforantes';
        $data['section'] = 'cheques';
        $data['editar'] = 'nuevo';
        $data['clientes'] = $this->cliente_model->get_clientes_drop();

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $this->cheque_model->id_cliente = $this->input->post('cliente');
            $this->cheque_model->fecha = $this->input->post('fecha');
            $this->cheque_model->monto = $this->input->post('monto');
            $this->cheque_model->numero = $this->input->post('numero');
            $this->cheque_model->banco = $this->input->post('banco');
            $this->cheque_model->observaciones = $this->input->post('observaciones');

            $result = $this->cheque_model->agregar_cheque();
            redirect('cheque/listado');
            exit;
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_cheque');
            $this->load->view('footer');
        }
    }

    public function editar($id)
    {
        $data['title'] = 'Editar Cheque - TEL - Autoperforantes';
        $data['description'] = 'Editar Cheque - TEL - Autoperforantes';
        $data['section'] = 'cheques';
        $data['editar'] = 'editar';
        $data['clientes'] = $this->cliente_model->get_clientes_drop();
        $data['cheque'] = $this->cheque_model->get_cheque($id);

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //editar
            $this->cheque_model->id_cliente = $this->input->post('cliente');
            $this->cheque_model->fecha = $this->input->post('fecha');
            $this->cheque_model->monto = $this->input->post('monto');
            $this->cheque_model->numero = $this->input->post('numero');
            $this->cheque_model->banco = $this->input->post('banco');
            $this->cheque_model->observaciones = $this->input->post('observaciones');

            $result = $this->cheque_model->editar_cheque($id);
            redirect('cheque/listado');
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_cheque');
            $this->load->view('footer');
        }
    }

    public function eliminar($id)
    {
        //eliminar articulos!!!
        $data['cheque'] = $this->cheque_model->eliminar_cheque($id);
        redirect('cheque/listado');
    }

}
