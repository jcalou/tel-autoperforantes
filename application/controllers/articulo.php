<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articulo extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('articulo_model');
        }

    public function listado()
    {
        $data['title'] = 'Listado de Art&iacute;culos - TEL - Autoperforantes';
        $data['description'] = 'Listado de Art&iacute;culos - TEL - Autoperforantes';
        $data['section'] = 'articulos';

        if($this->input->get('q')) {
            if($this->input->get('q')!="_stock") {
                $data['articulos'] = $this->articulo_model->buscar_articulos($this->input->get('q'));
                $data['q'] = $this->input->get('q');

                $this->load->view('header', $data);
                $this->load->view('listado_articulos', $data);
                $this->load->view('footer');
            }else{
                $data['articulos'] = $this->articulo_model->buscar_articulos_stock();
                $data['q'] = "Art&iacute;culos en stock";

                $this->load->view('header', $data);
                $this->load->view('listado_articulos', $data);
                $this->load->view('footer');
            }
        }else{
            $data['articulos'] = $this->articulo_model->get_articulos();

            $this->load->view('header', $data);
            $this->load->view('listado_articulos', $data);
            $this->load->view('footer');
        }

    }

    public function nuevo()
    {
        $data['title'] = 'Cargar nuevo Art&iacute;culo - TEL - Autoperforantes';
        $data['description'] = 'Cargar nuevo Art&iacute;culo - TEL - Autoperforantes';
        $data['section'] = 'articulos';
        $data['editar'] = 'nuevo';

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //agregar
            $this->articulo_model->codigo = $this->input->post('codigo');
            $this->articulo_model->tipo = $this->input->post('tipo');
            $this->articulo_model->categoria = $this->input->post('categoria');
            $this->articulo_model->nombre = $this->input->post('nombre');
            $this->articulo_model->tipo_pack = $this->input->post('tipo_pack');
            $this->articulo_model->cantidad = $this->input->post('cantidad');
            $this->articulo_model->precio_ref = $this->input->post('precio_ref');
            $this->articulo_model->stock = $this->input->post('stock');

            $result = $this->articulo_model->agregar_articulo();
            redirect('articulo/listado');
            exit;
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_articulo');
            $this->load->view('footer');
        }
    }

    public function editar($id)
    {
        $data['title'] = 'Editar Art&iacute;culo - TEL - Autoperforantes';
        $data['description'] = 'Editar Art&iacute;culo - TEL - Autoperforantes';
        $data['section'] = 'articulos';
        $data['editar'] = 'editar';
        $data['articulo'] = $this->articulo_model->get_articulo($id);

        if($this->input->post('post') && $this->input->post('post')=="1"){
            //editar
            $this->articulo_model->codigo = $this->input->post('codigo');
            $this->articulo_model->tipo = $this->input->post('tipo');
            $this->articulo_model->categoria = $this->input->post('categoria');
            $this->articulo_model->nombre = $this->input->post('nombre');
            $this->articulo_model->tipo_pack = $this->input->post('tipo_pack');
            $this->articulo_model->cantidad = $this->input->post('cantidad');
            $this->articulo_model->precio_ref = $this->input->post('precio_ref');
            $this->articulo_model->stock = $this->input->post('stock');

            $result = $this->articulo_model->editar_articulo($id);
            redirect('index.php/articulo/listado');
        }else{
            $this->load->view('header', $data);
            $this->load->view('nuevo_articulo');
            $this->load->view('footer');
        }
    }

    public function eliminar($id)
    {
        //eliminar articulos!!!
        $data['articulo'] = $this->articulo_model->eliminar_articulo($id);
        redirect('articulo/listado');
    }

    public function imprimir()
    {
        $data['title'] = 'Imprimir Listado de Art&iacute;culos - TEL - Autoperforantes';
        $data['description'] = 'Imprimir Listado de Art&iacute;culos - TEL - Autoperforantes';
        $data['section'] = 'articulos';
        $data['articulos'] = $this->articulo_model->get_articulos();

        $this->load->view('header', $data);
        $this->load->view('imprimir_articulo', $data);
        $this->load->view('footer');

    }

    public function imprimir_todos()
    {
        $data['articulos'] = $this->articulo_model->get_articulos();

        $this->load->helper('pdf_helper');
        $this->load->view('pdf_articulos', $data);

    }

    public function imprimir_stock()
    {
        $data['articulos'] = $this->articulo_model->buscar_articulos_stock();

        $this->load->helper('pdf_helper');
        $this->load->view('pdf_articulos', $data);

    }

    public function ajax_precio($id)
    {
        if ($id ==0){
            echo "0";
        }else{
            $articulo = $this->articulo_model->get_articulo($id);
            echo $articulo[0]->precio_ref;
        }
    }

    public function ajax_coeficiente($id)
    {
        if ($id == 0){
            echo "0";
        }else{
            $articulo = $this->articulo_model->get_articulo_coeficiente($id);
            echo $articulo[0]->coeficiente . ',' . $articulo[0]->tipo;
        }
    }

}
