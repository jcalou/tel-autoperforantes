<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('config_model');
    }
    public function index(){
        $data['title'] = 'TEL - Autoperforantes';
        $data['description'] = 'TEL - Autoperforantes';
        $data['section'] = 'dashboard';

        $data['coeficientes'] = $this->config_model->get_coeficientes();

        $this->load->view('header', $data);
        $this->load->view('dashboard', $data);
        $this->load->view('footer');
    }

}
