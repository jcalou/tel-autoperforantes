<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Presupuesto extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('presupuesto_model');
        $this->load->model('cliente_model');
        $this->load->model('articulo_model');
        $this->load->model('venta_model');
        }

    public function listado()
    {
        $data['title'] = 'Listado de presupuestos - TEL - Autoperforantes';
        $data['description'] = 'Listado de presupuestos - TEL - Autoperforantes';
        $data['section'] = 'presupuestos';

        $data['presupuestos'] = $this->presupuesto_model->get_presupuestos();

        $this->load->view('header', $data);
        $this->load->view('listado_presupuestos', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $data['title'] = 'Cargar nuevo Presupuesto - TEL - Autoperforantes';
        $data['description'] = 'Cargar nuevo Presupuesto - TEL - Autoperforantes';
        $data['section'] = 'presupuestos';
        $data['editar'] = 'nuevo';
        $data['clientes'] = $this->cliente_model->get_clientes_drop();
        $data['articulos'] = $this->articulo_model->get_articulos_drop();

        $this->load->view('header', $data);
        $this->load->view('nuevo_presupuesto');
        $this->load->view('footer');
    }

    public function ver($id)
    {
        $data['title'] = 'Ver Presupuesto - TEL - Autoperforantes';
        $data['description'] = 'Ver Presupuesto - TEL - Autoperforantes';
        $data['section'] = 'presupuestos';
        $data['presupuesto'] = $this->presupuesto_model->get_presupuesto($id);
        $data['presupuesto_detalle'] = $this->presupuesto_model->get_presupuesto_detalle($id);

        $this->load->view('header', $data);
        $this->load->view('ver_presupuesto', $data);
        $this->load->view('footer');
    }

    public function generarfactura($id)
    {
        $presupuesto = $this->presupuesto_model->get_presupuesto($id);
        $presupuesto_detalle = $this->presupuesto_model->get_presupuesto_detalle($id);

        $this->venta_model->id_cliente = $presupuesto[0]->id_cliente;
        $this->venta_model->des_auto = $presupuesto[0]->des_auto;
        $this->venta_model->des_bul = $presupuesto[0]->des_bul;
        $this->venta_model->des_tir = $presupuesto[0]->des_tir;
        $this->venta_model->des_ofe = $presupuesto[0]->des_ofe;
        $this->venta_model->fecha = $presupuesto[0]->fecha;
        $this->venta_model->iva = $presupuesto[0]->iva;
        $this->venta_model->observaciones = $presupuesto[0]->observaciones;
        $this->venta_model->total = $presupuesto[0]->total;

        $nueva_venta_id = $this->venta_model->agregar_venta();

        for($i=0;$i<count($presupuesto_detalle);$i++) {
            $this->venta_model->id_venta = $nueva_venta_id;
            $this->venta_model->id_articulo = $presupuesto_detalle[$i]->id_articulo;
            $this->venta_model->nombre_articulo = $presupuesto_detalle[$i]->nombre_articulo;
            $this->venta_model->cantidad = $presupuesto_detalle[$i]->cantidad;
            $this->venta_model->total = $presupuesto_detalle[$i]->total;

            $agregar = $this->venta_model->agregar_venta_detalle();
        }
        redirect('venta/listado');
        exit;

    }

    public function imprimir($id)
    {
        $data['presupuesto'] = $this->presupuesto_model->get_presupuesto($id);
        $data['presupuesto_detalle'] = $this->presupuesto_model->get_presupuesto_detalle($id);

        $this->load->helper('pdf_helper');
        $this->load->view('imprimir_presupuesto', $data);
    }

    public function eliminar($id)
    {
        $data['presupuesto'] = $this->presupuesto_model->eliminar_presupuesto($id);
        redirect('presupuesto/listado');
    }

    public function ajax_agregar()
    {
        $this->presupuesto_model->id_cliente = $this->input->post('id_cliente');
        $this->presupuesto_model->des_auto = $this->input->post('des_auto');
        $this->presupuesto_model->des_bul = $this->input->post('des_bul');
        $this->presupuesto_model->des_tir = $this->input->post('des_tir');
        $this->presupuesto_model->des_ofe = $this->input->post('des_ofe');
        $this->presupuesto_model->fecha = $this->input->post('fecha');
        $this->presupuesto_model->iva = $this->input->post('iva');
        $this->presupuesto_model->observaciones = $this->input->post('observaciones');
        $this->presupuesto_model->total = $this->input->post('total');

        $agregar = $this->presupuesto_model->agregar_presupuesto();

        echo $agregar;
    }

    public function ajax_agregar_detalle()
    {
        $this->presupuesto_model->id_presupuesto = $this->input->post('id_presupuesto');
        $this->presupuesto_model->id_articulo = $this->input->post('id_articulo');
        $this->presupuesto_model->nombre_articulo = $this->input->post('nombre_articulo');
        $this->presupuesto_model->cantidad = $this->input->post('cantidad');
        $this->presupuesto_model->total = $this->input->post('total');

        $agregar = $this->presupuesto_model->agregar_presupuesto_detalle();

        echo $agregar;
    }

}
