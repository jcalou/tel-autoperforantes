<?php
function drop_clientes($clientes,$id=0,$req,$id_tag="cliente") {
  $required="";
  if($req=="required"){$required='class="required"';}
  $select = '<select name="cliente" id="'.$id_tag.'" '.$required.'><option value="0">Seleccione un cliente</option>';
  for ($row = 0; $row < count($clientes); $row++){
    if ($id==$clientes[$row]->id){
      $select = $select . '<option selected="selected" value="'.$clientes[$row]->id.'">'.$clientes[$row]->nombre.'</option>';
    }else{
      $select = $select . '<option value="'.$clientes[$row]->id.'">'.$clientes[$row]->nombre.'</option>';
    }
  }
  $select = $select . '</select>';
  return $select;
}
function drop_articulos($articulos,$id=0,$req,$id_tag="articulo") {
  $required="";
  if($req=="required"){$required='class="required"';}
  $select = '<select name="articulo" id="'.$id_tag.'" '.$required.'><option value="0">Seleccione una articulo</option>';
  for ($row = 0; $row < count($articulos); $row++){
    if ($id==$articulos[$row]->id){
      $select = $select . '<option selected="selected" value="'.$articulos[$row]->id.'">'.$articulos[$row]->nombre.'</option>';
    }else{
      $select = $select . '<option value="'.$articulos[$row]->id.'">'.$articulos[$row]->nombre.'</option>';
    }
  }
  $select = $select . '</select>';
  return $select;
}

function drop_localidades($localidades,$id=0,$req,$id_tag="localidad") {
  $required = "";
  if($req == "required"){$required='class="required"';}
  $select = '<select name="localidad" id="'.$id_tag.'" '.$required.'><option value="-">Seleccione localidad</option>';
  for ($row = 0; $row < count($localidades); $row++){
    if ($id==$localidades[$row]->id){
      $select = $select . '<option selected="selected" value="'.$localidades[$row]->localidad.'">'.$localidades[$row]->localidad.'('.$localidades[$row]->cp.')</option>';
    }else{
      $select = $select . '<option value="'.$localidades[$row]->localidad.'">'.$localidades[$row]->localidad.'('.$localidades[$row]->cp.')</option>';
    }
  }
  $select = $select . '</select>';
  return $select;
}