<?php
function nombre_cliente($id=0) {
  $CI = get_instance();
  $CI->load->model('cliente_model');
  $cliente = $CI->cliente_model->get_cliente($id);
  return $cliente[0]->nombre;
}

function cobrado($id=0) {
  $CI = get_instance();
  $CI->load->model('cobro_model');
  $cobro = $CI->cobro_model->get_cobro($id);
  return "$ ".$cobro[0]->cobrado;
}

function nombre_localidad($id=0) {
  $CI = get_instance();
  $CI->load->model('localidad_model');
  $localidad = $CI->localidad_model->get_cliente($id);
  return $cliente[0]->nombre;
}

function facturado_cliente($id=0) {
  $CI = get_instance();
  $CI->load->model('venta_model');
  $venta = $CI->venta_model->get_venta_cliente($id);
  return "$ ".number_format($venta[0]->total,2,",",".");
}

function cobrado_cliente($id=0) {
  $CI = get_instance();
  $CI->load->model('cobro_model');
  $cobro = $CI->cobro_model->get_cobro_cliente($id);
  return "$ ".number_format($cobro[0]->cobrado,2,",",".");
}
