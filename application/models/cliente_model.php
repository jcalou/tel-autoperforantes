<?php
class cliente_model extends CI_Model
{
    var $id = "";
    var $nombre = "";
    var $direccion = "";
    var $localidad = "";
    var $telefono = "";
    var $d1 = "";
    var $d2 = "";
    var $d3 = "";
    var $d4 = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function get_clientes()
    {
        $query = "SELECT * from cliente";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_clientes_drop()
    {
        $query = "SELECT * from cliente ORDER BY nombre";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_cliente($id)
    {
        $query = "SELECT * from cliente WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function buscar_clientes($q)
    {
        $query = "SELECT * from cliente WHERE nombre LIKE '%" . $q . "%'";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_cliente()
    {
        $this->db->insert('cliente',array(
            'nombre'=> $this->nombre,
            'direccion'=> $this->direccion,
            'localidad'=> $this->localidad,
            'telefono'=> $this->telefono,
            'd1'=> $this->d1,
            'd2'=> $this->d2,
            'd3'=> $this->d3,
            'd4'=> $this->d4
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_cliente($id)
    {
        $this->db->where('id', $id);
        $this->db->update('cliente',array(
            'nombre'=> $this->nombre,
            'direccion'=> $this->direccion,
            'localidad'=> $this->localidad,
            'telefono'=> $this->telefono,
            'd1'=> $this->d1,
            'd2'=> $this->d2,
            'd3'=> $this->d3,
            'd4'=> $this->d4
        ));
    }

    public function eliminar_cliente($id)
    {
        $this->db->delete('cliente', array('id' => $id));
    }


}
