<?php
class articulo_model extends CI_Model
{
    var $id = "";
    var $codigo = "";
    var $tipo = ""; //Autoperforantes, Bulones, Tirafondos, Ofertas
    var $categoria = "";// TEL-HEX, TEL-DRY, TEL-ALAS, TEL-PHIL, TEL-FIX, TEL-PARKER, TEL-WALL, TARUGOS-TEL, BULONES
    var $nombre = "";
    var $tipo_pack = "";// Caja Grande, Estuche, Mini Estuche, Ahorro Pack
    var $cantidad = 0;
    var $precio_ref = "";
    var $stock = 0;
    var $stock_minimo = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_articulos()
    {
        $query = "SELECT * from articulo";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_articulos_drop()
    {
        $query = "SELECT * from articulo ORDER BY nombre";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_articulo($id)
    {
        $query = "SELECT * from articulo WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

     public function get_articulo_coeficiente($id)
    {
        $query = "SELECT a.*,t.coeficiente as coeficiente from articulo a LEFT JOIN tipo t ON a.tipo = t.nombre WHERE a.id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function buscar_articulos($q)
    {
        $query = "SELECT * from articulo WHERE nombre LIKE '%" . $q . "%'";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function buscar_articulos_stock()
    {
        $query = "SELECT * from articulo WHERE stock > 0";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_articulo()
    {
        $this->db->insert('articulo',array(
            'codigo'=> $this->codigo,
            'tipo'=> $this->tipo,
            'categoria'=> $this->categoria,
            'nombre'=> $this->nombre,
            'tipo_pack'=> $this->tipo_pack,
            'cantidad'=> $this->cantidad,
            'precio_ref'=> $this->precio_ref,
            'stock'=> $this->stock,
            'stock_minimo'=> $this->stock_minimo
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_articulo($id)
    {
        $this->db->where('id', $id);
        $this->db->update('articulo',array(
            'codigo'=> $this->codigo,
            'tipo'=> $this->tipo,
            'categoria'=> $this->categoria,
            'nombre'=> $this->nombre,
            'tipo_pack'=> $this->tipo_pack,
            'cantidad'=> $this->cantidad,
            'precio_ref'=> $this->precio_ref,
            'stock'=> $this->stock,
            'stock_minimo'=> $this->stock_minimo
        ));
    }

    public function eliminar_articulo($id)
    {
        $this->db->delete('articulo', array('id' => $id));
    }

    //reportes
    public function productos_en_stock()
    {
        $query = "SELECT COUNT(*) as dif, sum(stock) as total from articulo WHERE stock>0";
        $sql = $this->db->query($query);
        return $sql->result();
    }

}
