<?php
class venta_model extends CI_Model
{
    var $id = 0;
    var $id_cliente = 0;
    var $des_auto = "";
    var $des_bul = "";
    var $des_tir = "";
    var $des_ofe = "";
    var $fecha_carga = "";
    var $fecha = "";
    var $iva = 0;
    var $observaciones = "";

    var $id_venta = 0;
    var $id_articulo = 0;
    var $nombre_articulo = "";
    var $cantidad = 0;
    var $total = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_ventas($desde, $hasta, $cliente)
    {

        $query = "SELECT p.*,c.nombre as nombre_cliente from venta p LEFT JOIN cliente c ON p.id_cliente = c.id ";
        $query .= " WHERE (1=1) ";
        if ($desde != "-"){
            $query .= " AND (fecha>='$desde 00:00:00') ";
        }
        if ($hasta != "-"){
            $query .= " AND (fecha<='$hasta 23:59:59') ";
        }
        if ($cliente != "0"){
            $query .= " AND (c.id = '$cliente') ";
        }
        $query .= " ORDER BY id DESC";
        $sql = $this->db->query($query);
        return $sql->result();
    }

     public function get_venta($id)
    {
        $query = "SELECT * from venta WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_venta_detalle($id)
    {
        $query = "SELECT * from venta_detalle WHERE id_venta=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_venta_cliente($id)
    {
        $query = "SELECT sum(total) as total from venta WHERE id_cliente=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_venta()
    {
        $this->db->insert('venta',array(
            'id_cliente'=> $this->id_cliente,
            'des_auto'=> $this->des_auto,
            'des_bul'=> $this->des_bul,
            'des_tir'=> $this->des_tir,
            'des_ofe'=> $this->des_ofe,
            'fecha_carga'=> date('Y-m-d H:i:s'),
            'fecha'=> $this->fecha,
            'iva'=> $this->iva,
            'observaciones'=> $this->observaciones,
            'total'=> $this->total
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

     public function agregar_venta_detalle()
    {
        $this->db->insert('venta_detalle',array(
            'id_venta'=> $this->id_venta,
            'id_articulo'=> $this->id_articulo,
            'nombre_articulo'=> $this->nombre_articulo,
            'cantidad'=> $this->cantidad,
            'total'=> $this->total,
            'fecha_carga'=> date('Y-m-d H:i:s')
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function eliminar_venta($id)
    {
        $this->db->delete('venta', array('id' => $id));
        $this->db->delete('venta_detalle', array('id_venta' => $id));
    }

    public function total_mensual($d)
    {
        // $d YYYY-MM
        $_d = explode("-", $d);
        $query = "SELECT sum(total) as total from venta WHERE fecha >= '".$_d[0]."-".$_d[1]."-01' AND fecha <= '".$_d[0]."-".$_d[1]."-31'";
        $sql = $this->db->query($query);
        return $sql->result();
    }


}
