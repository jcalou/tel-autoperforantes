<?php
class cobro_model extends CI_Model
{
    var $id = 0;
    var $id_venta = 0;
    var $fecha = "";
    var $descripcion = "";
    var $cobrado = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_cobro($id)
    {
        $query = "SELECT sum(cobrado) as cobrado from cobro WHERE id_venta=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_cobro_cliente($id)
    {
        $query = "SELECT sum(cobrado) as cobrado from cobro c JOIN venta v ON c.id_venta=v.id WHERE id_cliente=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_cobro_lista($id)
    {
        $query = "SELECT * from cobro WHERE id_venta=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_cobro()
    {
        $this->db->insert('cobro',array(
            'id_venta'=> $this->id_venta,
            'fecha'=> $this->fecha,
            'descripcion'=> $this->descripcion,
            'cobrado'=> $this->cobrado
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function eliminar_cobro($id)
    {
        $this->db->delete('cobro', array('id' => $id));
    }


}
