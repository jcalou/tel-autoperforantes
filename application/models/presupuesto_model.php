<?php
class presupuesto_model extends CI_Model
{
    var $id = 0;
    var $id_cliente = 0;
    var $des_auto = "";
    var $des_bul = "";
    var $des_tir = "";
    var $des_ofe = "";
    var $fecha_carga = "";
    var $fecha = "";
    var $iva = 0;
    var $observaciones = "";

    var $id_presupuesto = 0;
    var $id_articulo = 0;
    var $nombre_articulo = "";
    var $cantidad = 0;
    var $total = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_presupuestos()
    {
        $query = "SELECT p.*,c.nombre as nombre_cliente from presupuesto p LEFT JOIN cliente c ON p.id_cliente = c.id ORDER BY id DESC";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_presupuesto($id)
    {
        $query = "SELECT * from presupuesto WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_presupuesto_detalle($id)
    {
        $query = "SELECT * from presupuesto_detalle WHERE id_presupuesto=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_presupuesto()
    {
        $this->db->insert('presupuesto',array(
            'id_cliente'=> $this->id_cliente,
            'des_auto'=> $this->des_auto,
            'des_bul'=> $this->des_bul,
            'des_tir'=> $this->des_tir,
            'des_ofe'=> $this->des_ofe,
            'fecha_carga'=> date('Y-m-d H:i:s'),
            'fecha'=> $this->fecha,
            'iva'=> $this->iva,
            'observaciones'=> $this->observaciones,
            'total'=> $this->total
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

     public function agregar_presupuesto_detalle()
    {
        $this->db->insert('presupuesto_detalle',array(
            'id_presupuesto'=> $this->id_presupuesto,
            'id_articulo'=> $this->id_articulo,
            'nombre_articulo'=> $this->nombre_articulo,
            'cantidad'=> $this->cantidad,
            'total'=> $this->total,
            'fecha_carga'=> date('Y-m-d H:i:s')
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function eliminar_presupuesto($id)
    {
        $this->db->delete('presupuesto', array('id' => $id));
        $this->db->delete('presupuesto_detalle', array('id_presupuesto' => $id));
    }


}
