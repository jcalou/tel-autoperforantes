<?php
class config_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_coeficientes()
    {
        $query = "SELECT * from tipo";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function modificar_coeficientes($c)
    {
        $this->db->where('id', 1);
        $this->db->update('tipo',array('coeficiente'=> $c[0]));
        $this->db->flush_cache();
        $this->db->where('id', 2);
        $this->db->update('tipo',array('coeficiente'=> $c[1]));
        $this->db->flush_cache();
        $this->db->where('id', 3);
        $this->db->update('tipo',array('coeficiente'=> $c[2]));
        $this->db->flush_cache();
        $this->db->where('id', 4);
        $this->db->update('tipo',array('coeficiente'=> $c[3]));
    }


    public function get_cps()
    {
        $query = "SELECT * from cp";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_cp($id)
    {
        $query = "SELECT * from cp WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_cp($loc,$cp)
    {
        $this->db->insert('cp',array(
            'cp'=> $cp,
            'localidad'=> $loc
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_cp($id,$loc,$cp)
    {
        $this->db->where('id', $id);
        $this->db->update('cp',array(
            'cp'=> $cp,
            'localidad'=> $loc
        ));
    }

    public function eliminar_cp($id)
    {
        $this->db->delete('cp', array('id' => $id));
    }


}
