<?php
class cheque_model extends CI_Model
{
    var $id = "";
    var $id_cliente = "";
    var $fecha = "";
    var $monto = 0;
    var $numero = "";
    var $banco = "";
    var $observaciones = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function get_cheques()
    {
        $query = "SELECT ch.*,c.nombre as nombre_cliente from cheque ch LEFT JOIN cliente c ON ch.id_cliente = c.id ORDER BY id DESC";
        $sql = $this->db->query($query);
        return $sql->result();
    }

     public function get_cheque($id)
    {
        $query = "SELECT * from cheque WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_cheque()
    {
        $this->db->insert('cheque',array(
            'id_cliente'=> $this->id_cliente,
            'fecha'=> $this->fecha,
            'monto'=> $this->monto,
            'numero'=> $this->numero,
            'banco'=> $this->banco,
            'observaciones'=> $this->observaciones
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function editar_cheque($id)
    {
        $this->db->where('id', $id);
        $this->db->update('cheque',array(
            'id_cliente'=> $this->id_cliente,
            'fecha'=> $this->fecha,
            'monto'=> $this->monto,
            'numero'=> $this->numero,
            'banco'=> $this->banco,
            'observaciones'=> $this->observaciones
        ));
    }

    public function eliminar_cheque($id)
    {
        $this->db->delete('cheque', array('id' => $id));
    }


}
