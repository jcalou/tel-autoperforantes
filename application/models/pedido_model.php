<?php
class pedido_model extends CI_Model
{
    var $id = 0;
    var $tipo = ""; //pedido, compra
    var $fecha_carga = "";
    var $fecha = "";
    var $iva = 0;
    var $observaciones = "";
    var $total = 0;

    var $id_pedido = 0;
    var $id_articulo = 0;
    var $nombre_articulo = "";
    var $cantidad = 0;
    var $precio = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_pedidos()
    {
        $query = "SELECT p.* from pedido p ORDER BY id DESC";
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_pedido($id)
    {
        $query = "SELECT * from pedido WHERE id=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function get_pedido_detalle($id)
    {
        $query = "SELECT * from pedido_detalle WHERE id_pedido=".$id;
        $sql = $this->db->query($query);
        return $sql->result();
    }

    public function agregar_pedido()
    {
        $this->db->insert('pedido',array(
            'tipo' => $this->tipo,
            'fecha_carga'=> date('Y-m-d H:i:s'),
            'fecha'=> $this->fecha,
            'iva'=> $this->iva,
            'observaciones'=> $this->observaciones,
            'total'=> $this->total
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function agregar_pedido_detalle()
    {
        $this->db->insert('pedido_detalle',array(
            'id_pedido'=> $this->id_pedido,
            'id_articulo'=> $this->id_articulo,
            'nombre_articulo'=> $this->nombre_articulo,
            'cantidad'=> $this->cantidad,
            'precio'=> $this->precio
        ));
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function eliminar_pedido($id)
    {
        $this->db->delete('pedido', array('id' => $id));
        $this->db->delete('pedido_detalle', array('id_pedido' => $id));
    }


}
