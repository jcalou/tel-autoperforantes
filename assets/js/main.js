function updateTotal() {
  var nuevototal = 0;
  $('#items').find('tbody tr').each(function(index, el) {
    var precio = $(this).children('td.item_preciototal').text().replace('$', '');
    var precionum = parseFloat(precio);
    nuevototal = nuevototal + precionum;
  });
  var subtotal = nuevototal.toFixed(2);
  var iva1 = subtotal * 0.21;
  var iva = iva1.toFixed(2);
  var total1 = parseFloat(subtotal) + parseFloat(iva);
  var total = total1.toFixed(2);

  $('#subtotal').text('$ ' + subtotal);
  $('#iva').text('$ ' + iva);
  $('#total').text('$ ' + total);
  return 0;
}

function updateArticuloTotal () {
  tot1_ =
  (parseFloat($('#preciounitario').val()).toFixed(2) * parseFloat($('#coeficiente').val()).toFixed(2) / 100) +
  parseFloat($('#preciounitario').val());
  console.log(tot1_);
  tot1 = 
  tot1_ - (tot1_ * parseFloat($('#coeficientecliente').val()).toFixed(2) / 100);
  console.log(tot1);
  //autoperforntes
  // console.log(parseFloat($('#preciounitario').val()).toFixed(2) * parseFloat($('#coeficiente').val()).toFixed(2) / 100);
  // console.log(parseFloat($('#preciounitario').val()));
  // console.log((parseFloat($('#preciounitario').val()).toFixed(2) * parseFloat($('#coeficientecliente').val()).toFixed(2) / 100));

  tot = tot1 * parseFloat($("#cantidad").val()).toFixed(2);
  console.log(tot);
$('#preciototal').val(tot.toFixed(2));   return 0; }

function pedido_updateTotal() {
  var nuevototal = 0;
  $('#items').find('tbody tr').each(function(index, el) {
    var precio = $(this).children('td.item_preciototal').text().replace('$', '');
    var precionum = parseFloat(precio);
    nuevototal = nuevototal + precionum;
  });
  var subtotal = nuevototal.toFixed(2);
  var iva1 = subtotal * 0.21;
  var iva = iva1.toFixed(2);
  var total1 = parseFloat(subtotal) + parseFloat(iva);
  var total = total1.toFixed(2);

  $('#pedido_subtotal').text('$ ' + subtotal);
  $('#pedido_iva').text('$ ' + iva);
  $('#pedido_total').text('$ ' + total);
  return 0;
}

function pedido_updateArticuloTotal () {
  tot1 = (parseFloat($('#pedido_preciounitario').val()).toFixed(2) * parseFloat($('#pedido_coeficiente').val()).toFixed(2) / 100) + parseFloat($('#pedido_preciounitario').val());
  tot2 = tot1 - tot1 * parseFloat($('#pedido_coeficientecliente').val()).toFixed(2) / 100;

  tot = tot2 * parseFloat($("#pedido_cantidad").val()).toFixed(2);
  $('#pedido_preciototal').val(tot.toFixed(2));
  return 0;
}

function rebindRemove () {
  $(".item_remove").on('click', function(e) {
    e.preventDefault();
    var row = $(this).parent('td').parent('tr');
    row.remove();
    updateTotal();
  });
}
$(document).ready(function(){

  // logica para reporte de gastos
  $('#reporte-gastos-dp1').fdatepicker({
    format: 'yyyy-mm-dd',
    language: 'es'
  });
  $('#reporte-gastos-dp2').fdatepicker({
    format: 'yyyy-mm-dd',
    language: 'es'
  });
  // fin logica para reporte de gastos

  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // PRESUPUESTO
  $('#dp3').fdatepicker({
    format: 'yyyy-mm-dd'
  });

  // limpiar el item
  $("#limpiaritem").click(function(event) {
    event.preventDefault();
    $("#cantidad").val("1");
    $("#articulo").val("0");
    $("#preciounitario").val("");
    $("#coeficiente").val("");
    $("#coeficientecliente").val("");
    $("#preciototal").val("");

  });

  // obtener descuento de cliente
  $("#cliente").bind('change', function(event) {
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: '/~juan/tel-autoperforantes/index.php/cliente/ajax_descuento/' + $("#cliente").val()
    })
    .done(function(msg) {
      var split = msg.split(",");
      $('#d1').val(split[0] + '%');
      $('#d2').val(split[1] + '%');
      $('#d3').val(split[2] + '%');
      $('#d4').val(split[3] + '%');
      updateTotal();
    })
    .fail(function() {console.log("error");})
  });

  // obtener precio de articulo
  $("#articulo").bind('change', function(event) {
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: '/~juan/tel-autoperforantes/index.php/articulo/ajax_precio/' + $("#articulo").val()
    })
    .done(function(msg) {
      $('#preciounitario').val(msg);
      $.ajax({
        type: "POST",
        url: '/~juan/tel-autoperforantes/index.php/articulo/ajax_coeficiente/' + $("#articulo").val()
      })
      .done(function(msg) {
        var split = msg.split(",");
        var desctemp = 0;
        $('#coeficiente').val(split[0]);
        switch(split[1]) {
          case 'Autoperforantes':
            desctemp = $('#d1').val();
            break;
          case 'Bulones':
            desctemp = $('#d2').val();
            break;
          case 'Tirafondos':
            desctemp = $('#d3').val();
            break;
          case 'Ofertas':
            desctemp = $('#d4').val();
            break;
          default:
              desctemp = 0;
        }
        $('#coeficientecliente').val(desctemp);
        _tempvalue = $('#articulo').val();
        $('#id_hidden').val(_tempvalue);
        updateArticuloTotal();
      })
      .fail(function() {console.log("error");})
    })
    .fail(function() {console.log("error");})

  });

  // recalcular precio en base a cantidad
  $("#cantidad").bind('change', function(event) {
    event.preventDefault();
    updateArticuloTotal();
  });

  // recalcular precio en base a precio unitario
  $("#preciounitario").bind('change', function(event) {
    event.preventDefault();
    updateArticuloTotal();
  });

  // agregar item
  $("#agregaritem").click(function(event) {
    event.preventDefault();
    var cantidad = $("#cantidad").val(),
    precio = 
      ((parseFloat($('#preciounitario').val()).toFixed(2) * parseFloat($('#coeficiente').val()).toFixed(2) / 100) 
      + parseFloat($('#preciounitario').val())) -
      ((parseFloat($('#preciounitario').val()).toFixed(2) * parseFloat($('#coeficiente').val()).toFixed(2) / 100) 
      + parseFloat($('#preciounitario').val()))
      * parseFloat($('#coeficientecliente').val()).toFixed(2) / 100,
    preciototal = parseFloat($("#preciototal").val()).toFixed(2),
    id = $("#id_hidden").val(),
    producto = $("#articulo  :selected").text();

    if (producto != "" && precio > 0 && preciototal > 0) {
      newrow = "<tr>";
      newrow = newrow + "<td class='td_nombre'>" + producto + "</td>";
      newrow = newrow + "<td class='td_precio'>$" + precio.toFixed(2) + "</td>";
      newrow = newrow + "<td class='td_cantidad'>" + cantidad + "</td>";
      newrow = newrow + "<td class='item_preciototal'>$" + preciototal + "</td>";
      newrow = newrow + "<td><a href='#' class='item_remove'><i class='fa fa-times'></i></a></td>";
      newrow = newrow + "<td class='item_id'>" + id + "</td>";
      newrow = newrow + "</tr>";

      $('#items').find('tbody').append(newrow);

      updateTotal();
      rebindRemove();

      $("#limpiaritem").click();

    }
  });

  var no_procesando_presupuesto = true;
  $('#aceptarpresupuesto').click(function(e) {
    e.preventDefault();
    if (no_procesando_presupuesto){
      //No se está procesando
      no_procesando_presupuesto = false;
      $('#aceptarpresupuesto').hide();
      $.ajax({
        type: "POST",
        url: "/~juan/tel-autoperforantes/index.php/presupuesto/ajax_agregar",
        data: {
          id_cliente: $('#cliente').val(),
          des_auto: $("#d1").val(),
          des_bul: $("#d2").val(),
          des_tir: $("#d3").val(),
          des_ofe: $("#d4").val(),
          fecha: $('#dp3').val(),
          iva: $('#iva').text().replace('$ ', ''),
          observaciones: $("#observaciones").val(),
          total: $('#total').text().replace('$ ', '')
        }
      })
        .done(function(msg) {
          //console.log("success:" + msg);

          var _id_presupuesto = msg;

          $('#items tbody tr').each(function(index, el) {

            var _id_articulo = $(this).children('td.item_id').text();
            var _nombre_articulo = $(this).children('td.td_nombre').text();
            var _cantidad = $(this).children('td.td_cantidad').text();
            var _total = $(this).children('td.item_preciototal').text().replace('$', '');

            $.ajax({
              type: "POST",
              url: "/~juan/tel-autoperforantes/index.php/presupuesto/ajax_agregar_detalle",
              data: {
                id_presupuesto: _id_presupuesto,
                id_articulo: _id_articulo,
                nombre_articulo: _nombre_articulo,
                cantidad: _cantidad,
                total: _total
              }
            })
            .done(function(msg) {
              //console.log(msg);
            })
            .fail(function() {
              //console.log("error");
            })
            .always(function() {
              //console.log("complete");
            });

          });
          window.location.href = '/~juan/tel-autoperforantes/index.php/presupuesto/listado';
        })
        .fail(function() {
          //console.log("error");
        })
        .always(function() {
          //console.log("complete");
        });
      }
    });

  // FIN PRESUPUESTO
  ///////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  // PEDIDO
  $('#pedido_dp').fdatepicker({
    format: 'yyyy-mm-dd'
  });

  // limpiar el item
  $("#pedido_limpiaritem").click(function(event) {
    event.preventDefault();
    $("#pedido_cantidad").val("1");
    $("#pedido_articulo").val("0");
    $("#pedido_preciounitario").val("");
    $("#pedido_coeficiente").val("");
    $("#pedido_coeficientecliente").val("");
    $("#pedido_preciototal").val("");

  });

  // obtener precio de articulo
  $("#pedido_articulo").bind('change', function(event) {
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: '/~juan/tel-autoperforantes/index.php/articulo/ajax_precio/' + $("#pedido_articulo").val()
    })
    .done(function(msg) {
      $('#pedido_preciounitario').val(msg);
      $.ajax({
        type: "POST",
        url: '/~juan/tel-autoperforantes/index.php/articulo/ajax_coeficiente/' + $("#pedido_articulo").val()
      })
      .done(function(msg) {
        var split = msg.split(",");
        var desctemp = 0;
        $('#pedido_coeficiente').val(split[0]);
        switch(split[1]) {
          case 'Autoperforantes':
            desctemp = 100/2.348; // (1 / 2.348) * 100
            break;
          case 'Bulones':
            desctemp = "45";
            break;
          case 'Tirafondos':
            desctemp = "45";
            break;
          case 'Ofertas':
            desctemp = "45";
            break;
          default:
              desctemp = 0;
        }
        $('#pedido_coeficientecliente').val(desctemp);
        _tempvalue = $('#pedido_articulo').val();
        $('#id_hidden').val(_tempvalue);
        pedido_updateArticuloTotal();
      })
      .fail(function() {console.log("error");})
    })
    .fail(function() {console.log("error");})

  });

  // recalcular precio en base a cantidad
  $("#pedido_cantidad").bind('change', function(event) {
    event.preventDefault();
    pedido_updateArticuloTotal();
  });

  // recalcular precio en base a precio unitario
  $("#pedido_preciounitario").bind('change', function(event) {
    event.preventDefault();
    pedido_updateArticuloTotal();
  });

  // agregar item
  $("#pedido_agregaritem").click(function(event) {
    event.preventDefault();
    var cantidad = $("#pedido_cantidad").val(),
    precio = parseFloat($("#pedido_preciototal").val()).toFixed(2) / cantidad,
    preciototal = parseFloat($("#pedido_preciototal").val()).toFixed(2),
    id = $("#id_hidden").val(),
    producto = $("#pedido_articulo :selected").text();

    if (producto != "" && precio > 0 && preciototal > 0) {
      newrow = "<tr>";
      newrow = newrow + "<td class='td_nombre'>" + producto + "</td>";
      newrow = newrow + "<td class='td_precio'>$" + precio.toFixed(2) + "</td>";
      newrow = newrow + "<td class='td_cantidad'>" + cantidad + "</td>";
      newrow = newrow + "<td class='item_preciototal'>$" + preciototal + "</td>";
      newrow = newrow + "<td><a href='#' class='item_remove'><i class='fa fa-times'></i></a></td>";
      newrow = newrow + "<td class='item_id'>" + id + "</td>";
      newrow = newrow + "</tr>";

      $('#items').find('tbody').append(newrow);

      pedido_updateTotal();
      rebindRemove();

      $("#limpiaritem").click();

    }
  });

var no_procesando_pedido = true;
  $('#pedido_aceptarventa').click(function(e) {
    e.preventDefault();
    if (no_procesando_pedido){
      //No se está procesando
      no_procesando_pedido = false;
      $('#pedido_aceptarventa').hide();
      $.ajax({
        type: "POST",
        url: "/~juan/tel-autoperforantes/index.php/pedido/ajax_agregar",
        data: {
          tipo: "pedido",
          fecha: $('#pedido_dp').val(),
          iva: $('#pedido_iva').text().replace('$ ', ''),
          observaciones: $("#pedido_observaciones").val(),
          total: $('#pedido_total').text().replace('$ ', '')
        }
      })
        .done(function(msg) {
          //console.log("success:" + msg);

          var _id_pedido = msg;

          $('#items tbody tr').each(function(index, el) {

            var _id_articulo = $(this).children('td.item_id').text();
            var _nombre_articulo = $(this).children('td.td_nombre').text();
            var _cantidad = $(this).children('td.td_cantidad').text();
            var _precio = $(this).children('td.item_preciototal').text();

            $.ajax({
              type: "POST",
              url: "/~juan/tel-autoperforantes/index.php/pedido/ajax_agregar_detalle",
              data: {
                id_pedido: _id_pedido,
                id_articulo: _id_articulo,
                nombre_articulo: _nombre_articulo,
                cantidad: _cantidad,
                precio: _precio
              }
            })
            .done(function(msg) {
              //console.log(msg);
            })
            .fail(function() {
              //console.log("error");
            })
            .always(function() {
              //console.log("complete");
            });

          });
          window.location.href = '/~juan/tel-autoperforantes/index.php/pedido/listado';
        })
        .fail(function() {
          //console.log("error");
        })
        .always(function() {
          //console.log("complete");
        });
      }
    });

  // FIN PEDIDO
  ///////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  // VENTA

  var no_procesando_venta = true;
  $('#aceptarventa').click(function(e) {
    e.preventDefault();
    if (no_procesando_venta){
      //No se está procesando
      no_procesando_venta = false;
      $('#aceptarventa').hide();
      $.ajax({
        type: "POST",
        url: "/~juan/tel-autoperforantes/index.php/venta/ajax_agregar",
        data: {
          id_cliente: $('#cliente').val(),
          des_auto: $("#d1").val(),
          des_bul: $("#d2").val(),
          des_tir: $("#d3").val(),
          des_ofe: $("#d4").val(),
          fecha: $('#dp3').val(),
          iva: $('#iva').text().replace('$ ', ''),
          observaciones: $("#observaciones").val(),
          total: $('#total').text().replace('$ ', '')
        }
      })
        .done(function(msg) {
          //console.log("success:" + msg);

          var _id_venta = msg;

          $('#items tbody tr').each(function(index, el) {

            var _id_articulo = $(this).children('td.item_id').text();
            var _nombre_articulo = $(this).children('td.td_nombre').text();
            var _cantidad = $(this).children('td.td_cantidad').text();
            var _total = $(this).children('td.item_preciototal').text().replace('$', '');

            $.ajax({
              type: "POST",
              url: "/~juan/tel-autoperforantes/index.php/venta/ajax_agregar_detalle",
              data: {
                id_venta: _id_venta,
                id_articulo: _id_articulo,
                nombre_articulo: _nombre_articulo,
                cantidad: _cantidad,
                total: _total
              }
            })
            .done(function(msg) {
              //console.log(msg);
            })
            .fail(function() {
              //console.log("error");
            })
            .always(function() {
              //console.log("complete");
            });

          });
          window.location.href = '/~juan/tel-autoperforantes/index.php/venta/listado';
        })
        .fail(function() {
          //console.log("error");
        })
        .always(function() {
          //console.log("complete");
        });
      }
    });

  // FIN VENTA
  ///////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
 //

 $('#cobro_dp').fdatepicker({
    format: 'yyyy-mm-dd'
  });

});
